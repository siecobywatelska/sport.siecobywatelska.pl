<?php
date_default_timezone_set('Europe/Warsaw');
define('WP_ADMIN', true);

    require_once('wp-load.php');
include_once('wp-admin/includes/image.php');


#if ( !current_user_can('upload_files') )
#    wp_die(__('You do not have permission to upload files.'));

$gmina = setiWatchdogUsers::getGmina();
#var_dump($gmina);
if ($gmina == null or (isset($gmina->nazwa_gminy) && $gmina->nazwa_gminy == '') )
{
//    header('HTTP/1.0 403 Relog');
    die('{"jsonrpc" : "2.0", "error" : {"code": 403, "message": "Please relog - your auth has been removed..."}, "id" : "id"}');
}

header('Content-Type: text/html; charset=' . get_option('blog_charset'));
/**
 * upload.php
 *
 * Copyright 2013, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 * 
 * @author Artur (Seti) Łabudziński - modified to fit needs of production and testing environment. Added DB support and security.
 */

#!! IMPORTANT: 
#!! this file is just an example, it doesn't incorporate any security checks and 
#!! is not recommended to be used in production environment as it is. Be sure to 
#!! revise it and customize to your needs.


// Make sure file is not cached (as it happens for example on iOS devices)
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");


if (isset($_REQUEST['command']))
{
    
    if ($_REQUEST['command'] == 'gmina_archive')
    {
        $gmina = setiWatchdogUsers::getGmina();
        if (!$gmina)
            die('Wymagane zalogowanie jako gmina');
        
        $gmina_id = $wpdb->get_results( $wpdb->prepare('select * from '.$wpdb->prefix.'users where user_login = %s', $gmina->nazwa_gminy) );
        if (count($$gmina_id) == 1)
            $gmina_id = $gmina_id[0];
        
        $ret = setiWatchdogExporter::generateGminaFile($gmina, $gmina_id, time().'-'.rand(1000,9999).'-'.$gmina->nazwa_gminy.'.zip', true, false);
        if (isset($ret['success']) && $ret['success'] == true)
        {
            Header('Location: '.$ret['file']);
            die();
        }
        else
            die('Generowanie pliku - nie powiodło się');
    }
    elseif ($_REQUEST['command'] == 'save')
    {
//        if (wp_verify_nonce($_REQUEST['nonce'], 'gmina-save-part-'.$_REQUEST['part']*1))
        {
            $state = setiWatchdogUsers::saveGminaAnkieta($_REQUEST['part'], $_REQUEST['content']);
            if ($state > 0) die( json_encode(array('status'=>true, 'message'=>'Zapisano!', 'id'=>$state, 'nonce'=>'')) );
            die('{status: false, message: "1Nie udało się zapisać zmiany. Skopiuj treść, odświerz stronę i spróbuj zapisać jeszcze raz."}');
        }
        #else
            die('{status: false, message: "2Nie udało się zapisać zmiany. Skopiuj treść, odświerz stronę i spróbuj zapisać jeszcze raz."}');
    }
    elseif ($_REQUEST['command'] == 'remove')
    {
        if (wp_verify_nonce($_REQUEST['nonce'], 'gmina-del-attachment-'.$_REQUEST['id']*1))
        {
            $state = setiWatchdogUsers::removeGminaFile(array('id'=>$_REQUEST['id'], 'part'=>$_REQUEST['part']));
            if ($state > 0) die( json_encode(array('id'=>$state)) );
            die('Could not delete object');
        }
        else
            die('Dont try to cheat!');
    }
}
/* 
// Support CORS
header("Access-Control-Allow-Origin: *");
// other CORS headers if any...
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    exit; // finish preflight CORS requests here
}
*/

// 5 minutes execution time
@set_time_limit(5 * 60);

// Uncomment this one to fake upload time
// usleep(5000);

// Settings
//$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
$targetDir = getcwd().'/front_uploads';
//$targetDir = 'uploads';
$cleanupTargetDir = true; // Remove old files
$maxFileAge = 5 * 3600; // Temp file age in seconds


// Create target dir
if (!is_dir($targetDir) && !file_exists($targetDir)) {
    @mkdir($targetDir);
}

// Get a file name
if (isset($_REQUEST["name"])) {
    $fileName = $_REQUEST["name"];
} elseif (!empty($_FILES)) {
    $fileName = $_FILES["file"]["name"];
} else {
    $fileName = uniqid("file_");
}

$filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;

// Chunking might be enabled
$chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


// Remove old temp files    
if ($cleanupTargetDir) {
    if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
    }

    while (($file = readdir($dir)) !== false) {
        $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

        // If temp file is current file proceed to the next
        if ($tmpfilePath == "{$filePath}.part") {
            continue;
        }

        // Remove temp file if it is older than the max age and is not the current file
        if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
            @unlink($tmpfilePath);
        }
    }
    closedir($dir);
}    


// Open temp file
//echo "{$filePath}.part";
if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
}

if (!empty($_FILES)) {
    if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
    }

    // Read binary input stream and append it to temp file
    if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
    }
} else {    
    if (!$in = @fopen("php://input", "rb")) {
        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
    }
}
//print_r($_FILES["file"]);
while ($buff = fread($in, 4096)) {
    fwrite($out, $buff);
}

@fclose($out);
@fclose($in);


    if (!is_dir($targetDir . DIRECTORY_SEPARATOR . $gmina->nazwa_gminy))
    {
        mkdir($targetDir . DIRECTORY_SEPARATOR . $gmina->nazwa_gminy);
        file_put_contents(' ',$targetDir . DIRECTORY_SEPARATOR . $gmina->nazwa_gminy . DIRECTORY_SEPARATOR . 'index.php');
        file_put_contents('
            <IfModule mod_rewrite.c>
                RewriteEngine On
                RewriteBase /
                RewriteRule ^index\.php$ - [L]
                RewriteCond %{REQUEST_FILENAME} !-f
                RewriteCond %{REQUEST_FILENAME} !-d
                RewriteRule . /index.php [R=404,L]
            </IfModule>',
        $targetDir . DIRECTORY_SEPARATOR . $gmina->nazwa_gminy . DIRECTORY_SEPARATOR . '.htaccess');
    }


// Check if file has been uploaded
if (!$chunks || $chunk == $chunks - 1) {
    // Strip the temp .part suffix off
    rename("{$filePath}.part", $filePath);

    //Get Extention
    $fileExt = pathinfo($filePath, PATHINFO_EXTENSION);
    rename($filePath, $targetDir . DIRECTORY_SEPARATOR . $gmina->nazwa_gminy . DIRECTORY_SEPARATOR . ($newFileName = md5(time() . mktime() . $filePath)) . '.'.$fileExt);

    
    $fileData = setiWatchdogUsers::getFileType($_FILES['file']['type'], $targetDir . DIRECTORY_SEPARATOR . $gmina->nazwa_gminy . DIRECTORY_SEPARATOR . $newFileName . '.'.$fileExt);
//    var_dump($_FILES);
//    var_dump($_REQUEST);

    $orgName = !empty($_FILES) ? (($_FILES["file"]['name'] == '' || $_FILES["file"]['name'] == 'blobl') ? ($fileName == 'blob' ? $_REQUEST['name'] : $fileName) : $_FILES["file"]['name']) : $fileName;
    if ($orgName == 'blob') $orgName = $_REQUEST['name']; 
    $array = array(
        'fileName' => $orgName,
        'realName' => $newFileName . '.'.$fileExt,
        'gmina'    => $gmina->nazwa_gminy,
        'path'     => $filePath,
        'type'     => $fileData['type'],
        'excerp'   => $fileData['excerp'],
        'menu'     => $_REQUEST['part'],
    );
//    var_dump($array);
    
    $fileID = setiWatchdogUsers::addGminaFile($array);
    
    echo    json_encode(array('jsonrpc'=>'2.0', 'result'=> null, 'id' => $fileID, 'href' => get_attachment_link($fileID), 'nonce'=>wp_create_nonce('gmina-del-attachment-'.$fileID)));
}
else
{ //Chunk!
    die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
}
