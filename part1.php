<?php
$_part = $title[2]*1;  //Part 1 A
//var_dump($_part);
//    wp_enqueue_script('plupload-handlers');
    $pageData = $post;
    $pageMeta = get_post_meta($pageData->ID);
    
//    var_dump($pageData);
//    var_dump($pageMeta);
?>
<article id="part_<?php echo $_part; ?>_A" class="partAB">
    <header>
        <h1 class="big"><?php echo $ankietyTitles[$_part]; ?></h1>
        <?php
            if (isset($pageMeta['obszary']))
            {
                ?>
                    <div>
                        <label>Obszary do zbadania</label>
                        <ul class="dokumenty">
                            <?php
                                if (!is_array($pageMeta['obszary'])) $pageMeta['obszary'] = array($pageMeta['obszary']);
                                foreach ($pageMeta['obszary'] as $doc)
                                {
                                    ?>
                                        <li><?php echo $doc; ?></li>
                                    <?php
                                }
                            ?>
                        </ul>
                    </div>
                <?php
            }
            
            if (isset($pageMeta['dokumenty']))
            {
                ?>
                    <div>
                        <label>Zanalizuj dokumenty</label>
                        <ul class="dokumenty">
                            <?php
                                if (!is_array($pageMeta['dokumenty'])) $pageMeta['dokumenty'] = array($pageMeta['dokumenty']);
                                foreach ($pageMeta['dokumenty'] as $doc)
                                {
                                    ?>
                                        <li><?php echo $doc; ?></li>
                                    <?php
                                }
                            ?>
                        </ul>
                    </div>
                <?php
            }
            
            if (isset($pageMeta['pytanie']))
            {
                ?>
                    <div>
                        <label>Znajdź odpowiedź na pytanie:</label>
                        <ul class="pytania">
                            <?php
                                if (!is_array($pageMeta['pytanie'])) $pageMeta['pytanie'] = array($pageMeta['pytanie']);
                                foreach ($pageMeta['pytanie'] as $doc)
                                {
                                    ?>
                                        <li><?php echo $doc; ?></li>
                                    <?php
                                }
                            ?>
                        </ul>
                    </div>
                <?php
            }
        ?>
    </header>
    <section class="file_upload_holder">
        <header>
            <h6 style="text-align: center;">Pliki</h6>
        
<?php
getGminaFiles($_part, $gmina);
//    writeUploaderHTMLforPart($_part);
?>
            <div class="clear"></div>
        </header>
        <div class="clear page_content">
  <?php if (!isset($pageMeta['notatnik'])) { ?><h4>Zanalizuj treść dokumentów</h4><?php } else { ?><h4><?php echo isset($pageMeta['force-title']) ? $pageMeta['force-title'][0] : 'Notatnik'; ?></h4><?php } ?>
            <?php
                    $content = apply_filters( 'the_content', $pageData->post_content );
                    $content = str_replace( ']]>', ']]&gt;', $content );   
                    echo $content;
            ?>
        </div>
    </section>
            <section class="editor" attr="<?php echo $_part; ?>">
                <header>
                    <?php
                        $tresc = getGminaAnkieta($_part, $gmina);
                        echo stripslashes($tresc->post_content)
                    ?>
                </header>
            </section>
<?php
//    writeGminaAnkietaEditor($_part);
?>
</article>
