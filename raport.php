<?php
date_default_timezone_set('Europe/Warsaw');
//define('WP_ADMIN', true);

    require_once('wp-load.php');
//include_once('wp-admin/includes/image.php');

$gmina = isset($_GET['gmina']) ? $_GET['gmina'] : '';

    function getGmina($gmina = '')
    {
        global $wpdb;
        global $current_user;
        $login_gmina = $gmina;
        $current_gmina = $wpdb->get_results( $wpdb->prepare('Select * from '.$wpdb->prefix.'gmina where nazwa_gminy = %s and isDeleted = 0', $login_gmina));

        if (count($current_gmina) > 0)
        {
            return $current_gmina[0];
            self::$gmina = $current_gmina[0];
        }
        else
        {
            return false;
            self::$gmina = false;
        }
        
        //return self::$gmina;
    }

    function getGminaAnkieta($part, $gmina)
    {
        global $wpdb;
        $gmina = getGmina($gmina->nazwa_gminy);
        $args = array(
            'post_type' => 'gmina_ankieta',
            'posts_per_page' => -1,
            'post_status' => 'any',
            //'post_title' => 'Ankieta:'.$gmina->nazwa_gminy.':'.($part*1),
        );
        $posts = new WP_Query( $args );
        if ( $posts -> have_posts() )
        {
            $posts = $posts->get_posts();
            foreach ($posts as $post)
            {
#                echo '<pre>'.$post->ID.' / '.$post->post_title.' / '.('Ankieta:'.$gmina->nazwa_gminy.':'.($part*1)).'</pre>';
#                print_r($post->post_title == 'Ankieta:'.$gmina->nazwa_gminy.':'.($part*1));
                if (trim($post->post_title) == 'Ankieta:'.$gmina->nazwa_gminy.':'.($part*1) )
                    return $post;
            }
        }
    }

    
        function getGminaFiles($_part, $gmina)
        {
                    $args = array(
                        'post_type' => 'gmina_file',
                        'posts_per_page' => -1,
                        'post_title' => $gmina->nazwa_gminy,
                        'menu_order' => $_part,
            'post_status' => 'any',
                        
                    );
                    $files = new WP_Query( $args );
                    if ( $files -> have_posts() )
                    {
                        $files = $files->get_posts();
                        
                        
            //            while ( $posts -> have_posts() )            
                        foreach ( $files as $file )            
                        {
//                            echo $file->post->title;
                            if ($file->menu_order == $_part && $file -> post_title == $gmina->nazwa_gminy)
                            {
//                                var_dump($file);
        //                    isset($i) ? ++$i : $i = 0;
                            ?>
                                    <div class="media-item child-of-0" id="media-item-<?php echo $file->ID; ?>">
                                        <div class="filename original" attr="<?php echo $file->ID; ?>"><a href="<?php echo ( get_permalink( $file->ID ) ); ?>"><?php echo $file->post_content_filtered; ?></a></div>
                                    </div>
                            <?php
                            }
                        }
                    }

        }

$raport_array = Array();
                    $args = array(
                        'post_type' => 'gmina_file',
                        'posts_per_page' => -1,
//                      'post_title' => $gmina,
                        'menu_order' => 999,
                        'post_status' => 'any',
                    );
                    $files = new WP_Query( $args );
                    if ( $files -> have_posts() )
                    {
                        foreach ($files->posts as $file)
                        {
                            if (is_string($file->post_content))
                            {
                             $raport_array[$file->post_title] = array('file'=>$file->post_content, 'name'=>'Raport '.$gmina . ' - ' . $file->post_content_filtered);
#                             break;
                            }
                        }
                    }
//var_dump($raport_array);
//echo '<pre>';
//var_Dump($files);
//echo '</pre>';
//                                        $files = new WP_Query( $args );
//                    if ( $files -> have_posts() )
//                    {
//                        foreach ($files as $file)
//                        {
//                            if (is_string($file->post_content)) $raport_array[$file->post_title] = $file->post_content;
//                        }
//                    }
//var_dump($raport_array);
//var_dump($gmina);
//r_dump($files);
if ($raport_array[$gmina]) 
{
    $file = $raport_array[$gmina];
//    echo 'FILE SENDING!: <a href="/front_uploads/'.$gmina.'/'.$file['file'].'">Plik</a>';
    $ankieta = isset($_GET['ankieta']) ? $_GET['ankieta']*1 : '';

    if ($ankieta > 0)
    {
        $gmina = getGmina($gmina);
//        var_Dump($gmina);
            $post = get_post($ankieta);

            $pageMeta = get_post_meta($post->ID);
            $titles = explode(' ', $post->post_title);
                $title = explode(' ',$post->post_title);
//            var_Dump($pageMeta);
            get_header();
            ?>
                <style>
                    .editor {
                        border: 1px solid black;
                        background: rgb(255,255,154);
                        text-align: justify;
                        padding: 5px;
                    }
                    .editor p, .editor li {
                        text-align: justify;
                        padding: 5px;
                    }
                    .editor ul {
                        margin: 5px 15px;
                    }
                </style>
            <?php
            if (isset($pageMeta['podsumowanie']) && ($pageMeta['podsumowanie'] == true || $pageMeta['podsumowanie'] == 'true' || $pageMEta['podsumowanie']*1 == 1))
                include('podsumowanie.php');
            else
            {
                include('part1.php');
                $pageMeta = get_post_meta($pageData->ID);
                if (!isset($pageMeta['notatnik']))
                {
                    include('part2.php');
                }
                else
                {
                    echo '<a href="'.esc_url(get_permalink( get_page_by_title( 'Questionarie 2013' ) )).'">Powrót do strony z ankietami</a>';
                }
            }
            get_footer();

    }
    else if ($ankieta === 0)
    {
        $g = getGmina($gmina);
        get_header();
        ?>
            <h1 style="font-size: 20px; text-transform: uppercase;text-align: center; margin:5px;   ">Dane zebrane w ramach OMS w gminie <em><?php echo $g->nazwa;?></em></h1>
                                                            <section class="float_left" style="margin-top:15px;">
                                                                <h3>Część I - Wpływ sportowej polityki gminy na życie mieszkańców</h3>
                                                                <ul class="ankieta_part">
                                                                    <li class="transparent"></li>
                                                                    <li><a href="/raport.php?gmina=<?php echo $gmina; ?>&ankieta=<?php $k = (( get_page_by_title( 'Questionarie 2013 1' ) )); echo $k->ID; ?>">Cele gminy w zakresie sportu</a></li>
                                                                    <li><a href="/raport.php?gmina=<?php echo $gmina; ?>&ankieta=<?php $k = (( get_page_by_title( 'Questionarie 2013 3' ) )); echo $k->ID; ?>">Zajęcia sportowe dla mieszkańców</a></li>
                                                                    <li><a href="/raport.php?gmina=<?php echo $gmina; ?>&ankieta=<?php $k = (( get_page_by_title( 'Questionarie 2013 5' ) )); echo $k->ID; ?>">Dostęp mieszkańców do obiektów sportowych</a></li>
                                                                    <li><a href="/raport.php?gmina=<?php echo $gmina; ?>&ankieta=<?php $k = (( get_page_by_title( 'Questionarie 2013 7' ) )); echo $k->ID; ?>">Informowanie mieszkańców o ofercie sportowej</a></li>
                                                                    <li><a href="/raport.php?gmina=<?php echo $gmina; ?>&ankieta=<?php $k = (( get_page_by_title( 'Questionarie 2013 9' ) )); echo $k->ID; ?>">Inne aspekty sportowej polityki gminy</a></li>
                                                                    <li class="transparent"></li>
                                                                    <!--
                                                                    <li><a href="/raport.php?gmina=<?php echo $gmina; ?>&ankieta=<?php $k = (( get_page_by_title( 'Questionarie 2013 11' ) )); echo $k->ID; ?>">Podsumowanie</a></li>
                                                                    -->
                                                                </ul>
                                                            </section>
                                                            <section class="float_right" style="margin-top:15px;">
                                                                <h3>Część II - Wpływ mieszkańców na politykę sportową gminy</h3>
                                                                <ul class="ankieta_part">
                                                                    <li class="transparent"></li>
                                                                    <li><a href="/raport.php?gmina=<?php echo $gmina; ?>&ankieta=<?php $k =  (( get_page_by_title( 'Questionarie 2013 21' ) )); echo $k->ID;  ?>">Badanie potrzeb i konsultacje społeczne</a></li>
                                                                    <li><a href="/raport.php?gmina=<?php echo $gmina; ?>&ankieta=<?php $k =  (( get_page_by_title( 'Questionarie 2013 23' ) ));  echo $k->ID; ?>">Wspieranie inicjatyw mieszkańców</a></li>
                                                                    <li class="transparent"></li>
                                                                    <!--
                                                                    <li><a href="</raport.php?gmina=<?php echo $gmina; ?>&ankieta=<?php $k =  (( get_page_by_title( 'Questionarie 2013 31' ) )); echo $k->ID;  ?>">Podsumowanie</a></li>
                                                                    -->
                                                                </ul>
                                                            </section>
                                                            <div class="clear"></div>
        <?php
        get_footer();
    }
    else if (file_exists('front_uploads/'.$gmina.'/'.$file['file']))
    {
            header("Content-type: application/octet-stream");
            header("Content-Transfer-Encoding: Binary");
            header("Content-length: " . filesize('front_uploads/'.$gmina.'/'.$file['file']));
            header('Content-disposition: attachment; filename=' . $file['name']);
            
            $handle=fopen('front_uploads/'.$gmina.'/'.$file['file'], 'rb');
            while (!feof($handle))
            {
                echo fread($handle, 8192);
                flush();
            }
            fclose($handle);        
    }
    else
    {
        echo 'Raport nie istnieje!';
    }
}
else
{
    echo 'Gmina nie istnieje - oszukujesz?';
}