
        </div><!-- #main -->
        <footer id="colophon" class="site-footer" role="contentinfo">
            <div id="footer_desc" class="float_left">
                Działanie realizowane jest w ramach projektu „Mocna straż – wzmocnienie działań kontrolnych organizacji, grup nieformalnych i obywateli/ek” Sieci obywatelskiej – Watchdog Polska, finansowane przez Szwajcarię w ramach szwajcarskiego programu współpracy z nowymi krajami członkowskimi Unii Europejskiej.
            </div>
            <img class="float_right" src="<?php echo get_template_directory_uri();?>/img/swisspoprawne.PNG"/>
            <div class="clear"></div>
        </footer><!-- #colophon -->
    </div><!-- #page -->

    <?php wp_footer(); ?>
    <script type="text/javascript">
        //Cufon.replace('label, h1, h2, h3, h4, h5, h5, input[type="submit"], input[type="button"], .drag-drop-info');
    </script>
</body>
</html>