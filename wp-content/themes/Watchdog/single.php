<?php get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
                        <div id="single_post">
                            <h1><?php the_title();?></h1>
                            <div id="page_content" class="page_content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                        <?php endwhile; // end of the loop. ?>    

		</div><!-- #content -->
	</div><!-- #primary -->

<?php // get_sidebar(); ?>
<?php get_footer(); ?>
a