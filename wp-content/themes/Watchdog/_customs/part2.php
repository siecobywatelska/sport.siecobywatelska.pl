<?php
//$_part = $title[2]+1;  //Part 1 A
$_part = $_part + 1;  //Part next... B
//var_dump($_part);

    wp_enqueue_script('plupload-handlers');
//Nadpisz ustawienia... Tak by Part2 mógł działać.

                    $args = array(
                        'post_type' => 'page',
                        'posts_per_page' => -1,
                        'post_title' => $title[0] . ' ' . $title[1] . ' ' . $_part,
                    );
                    $posts = new WP_Query( $args );
                    if ( $posts -> have_posts() )
                    {
                        $posts = $posts->get_posts();
                        foreach ($posts as $tmp)
                        {
                            if ($tmp->post_title == $title[0] . ' ' . $title[1] . ' ' . $_part)
                                $pageData = $tmp;
                        }
                        //$post = $tmp;
                    }
//    $pageData = $post;
    $pageMeta = get_post_meta($pageData->ID);

    if ($post->ID == $pageData->ID)
        die('Błąd');

//    var_dump($pageData);
//    var_dump($pageMeta);
?>
<article id="part_<?php echo $_part; ?>_A" class="partAB">
    <header>
        <div>
            <h4>Zanalizuj, jak zapisy dokumentu wpływają na życie mieszkańców</h4>
        </div>
    </header>
    <section class="file_upload_holder">
        <header>
<?php
//    writeUploaderHTMLforPart($_part);
?>
        </header>
        <div class="clear page_content">
            <?php
                    $content = apply_filters( 'the_content', $pageData->post_content );
                    $content = str_replace( ']]>', ']]&gt;', $content );   
                    echo $content;
            ?>
        </div>
    </section>
<?php
    writeGminaAnkietaEditor($_part);
?>
</article>
<a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013' ) )); ?>">Powrót do strony z ankietami</a>