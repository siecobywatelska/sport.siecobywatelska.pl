<?php
$title = explode(' ',$post->post_title);

get_header();


?>
    <h2>Pliki na rok <?php echo $title[1]; ?></h2>
    <div class="clear">
        <?php
                $content = apply_filters( 'the_content', $post->post_content );
                $content = str_replace( ']]>', ']]&gt;', $content );   
                echo $content;
        ?>
    </div>
    
    <div class="pliki">
        <ul>
            <?php
                $atts = get_children( array (
                    'post_parent' => $post->ID,
                    'post_type' => 'attachment',
                ));
                
                if (!empty($atts))
                {
                    foreach ($atts as $at)
                    {
                        ?>
                            <li class="clear">
                                <div class="type" style="width: 150px;float: left;"><span>Plik: </span><?php echo $at->post_mime_type; ?></div>
                                <div class="link" style="width: 100px;float: left;"><a href="<?php echo $at->guid; ?>">Pobierz</a></div>
                                <div class="title" style="float: left;"><?php echo $at->post_title; ?></div>
                            </li>
                        <?php
                    }
                }
                else
                {
                    echo '<li class="clear">Brak plików przygotowanych przez grupę Watchdog na rok '.$title[1].'</li>';
                }
            ?>
            <li class="clear"> </li>
        </ul>
    </div>
<?php
get_footer();
