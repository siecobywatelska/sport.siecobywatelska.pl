<script type="text/javascript">
//Ogólne skrypty

    //Skrypt zapisujący aktualnego editora (wybranego z save buttona).
    jQuery(document).ready( function () {
        jQuery('.editor_buttons .save').click(function (i,o) {
            var that = this;
            if (jQuery(this).hasClass('ajax')) return;
            
            jQuery.ajax({
                url: '/test.php',
                type: 'POST',
                dataType: 'json',
                cache: false,
                data: {command: 'save', nonce: jQuery(that).attr('nonce'), part: jQuery(that).attr('attr'), 'content': tinyMCE.get('editor_' + jQuery(that).attr('attr')).getContent() },
                beforeSend: function () { jQuery(that).addClass('ajax'); jQuery('.editor[attr="' + jQuery(that).attr('attr') + '"] .message').html('Zapisywanie.....').fadeIn(200); },
                complete: function () { jQuery(that).removeClass('ajax'); },
                success: function (ret) {
                    console.log(ret);
                    jQuery('.editor[attr="' + jQuery(that).attr('attr') + '"] .message').html(ret.message);//.fadeIn(200);
                    jQuery(that).attr('nonce', ret.nonce);
                    setTimeout(function () {jQuery('.editor[attr="' + jQuery(that).attr('attr') + '"] .message').fadeOut(1000);}, 1500);
//                    setTimeout(function () {jQuery('.editor[attr="' + jQuery(that).attr('attr') + '"] .message').html('&nbsp;');}, 1500);
                }
            });
        });
    });
    
    //Skrypt który usuwa odpowiedni plik
    jQuery(document).on('click', '.uploader_list .remove', function (o) {
        var that = this;
//        console.log('Remove Video: ' + jQuery(this).attr('attr'));
        jQuery.ajax({
            cache: false,
            complete: function () {
//                jQuery(that).addClass('pointer');
            },
            data: {command: 'remove', id: jQuery(that).attr('attr'), nonce: jQuery(that).attr('nonce'), 'part': jQuery(that).attr('menu')},
            dataType: 'json',
            success: function (ret) {
                try {
//                    console.log(ret);
                    jQuery('#media-item-' + ret.id).fadeOut();
                    jQuery('.media-item[postid="' + ret.id + '"]').fadeOut();
                } catch (e) {
                    
                }
            },
            type: 'POST',
            url: '/test.php'
        });
    });
    
    // Dodaje plik do kolejki (tworzy jego html'a do późniejszej zabawy)
    function watchdog_fileQueued(fileObj, part) {
        console.log('Adding: ');
        console.log(fileObj);
        console.log('Part: ' + part);
        // Get rid of unused form
        jQuery('.media-blank').remove();

        var items = jQuery('#media-items' + part).children(), postid = post_id || 0;

        // Collapse a single item
        if ( items.length == 1 ) {
            items.removeClass('open').find('.slidetoggle').slideUp(200);
        }
        // Create a progress bar containing the filename
        jQuery('<div class="media-item">')
            .attr( 'id', 'media-item-' + fileObj.id )
            .addClass('child-of-' + postid)
            .append('<div class="progress"><div class="percent">0%</div><div class="bar"></div></div>',
                jQuery('<div class="filename original">').text( ' ' + fileObj.name ))
            .appendTo( jQuery('#media-items' + part ) );

        // Disable submit
        jQuery('#insert-gallery').prop('disabled', true);
    }
</script>
<script type="text/javascript">
var post_id = 0;
var uploaders = [];

jQuery(document).ready ( function () {
     jQuery(".file_upload_holder .uploader").each( function (i,o) {
        var part = jQuery(o).attr('attr');
        console.log(part);
        console.log(o);
        console.log(jQuery(o));
        var uploader = new plupload.Uploader({
        runtimes : 'html5,flash',
        browse_button : 'plupload-browse-button' + part,
        url: '/test.php',
        flash_swf_url : '/plupload/js/plupload.flash.swf',
        chunk_size: '1m',
        unique_names: true,
            'flash_swf_url' : '<?php echo includes_url('js/plupload/plupload.flash.swf'); ?>',
            'silverlight_xap_url' : '<?php echo includes_url('js/plupload/plupload.silverlight.xap'); ?>',
        //    'filters' => array( array('title' => __( 'Allowed Files' ), 'extensions' => '*') ),
            'multipart' : true,
            'urlstream_upload' : true,
            drop_element : 'drag-drop-area' + part,
            'multipart_params' : {'part': part} //To to wysyła dodatkowe parametry postem z chunkami^_^ 
        });
        uploader.part = part;
    
        uploader.bind('Init', function(up, params) {
            if ( up.features.dragdrop && ! jQuery(document.body).hasClass('mobile') ) {
                var target = jQuery("#drag-drop-area");

                target.ondragover = function(event) {
                    event.dataTransfer.dropEffect = "copy";
                };

                target.ondragenter = function() {
                    this.className = "dragover";
                };

                target.ondragleave = function() {
                    this.className = "";
                };

                target.ondrop = function() {
                    this.className = "";
                };
            }
        });

        uploader.bind('FileUploaded', function (up, file, ret) {
            try {
                var data = JSON.parse(ret.response);
                jQuery('#media-item-' + file.id).attr('postID', data.id);
                jQuery('<div class="remove" nonce="' + data.nonce + '" menu="' + <?php echo $_part; ?> + '" attr="' + data.id + '">Usuń</div>')
                    .appendTo('#media-item-' + file.id);
                jQuery('#media-item-' + file.id + ' .filename').html('<a href="' + data.href + '">' + jQuery('#media-item-' + file.id + ' .filename').html() + '</a>');
                jQuery('#media-item-' + file.id + ' .progress').fadeOut(1000);
            } catch (e) {}
        });

        uploader.bind('ChunkUploaded', function (up, file, ret) {
            try {
            } catch (e) {}
        });

        uploader.bind('Error', function (up, obj) {
            console.log('Error');
            console.log(up);
            console.log(obj);
        });


        uploader.bind('FilesAdded', function(up, files) {
            for (var i in files) {
                watchdog_fileQueued(files[i], up.part);
                //jQuery('#filelist').innerHTML += '<div id="' + files[i].id + '">' + files[i].name + ' (' + plupload.formatSize(files[i].size) + ') <b></b></div>';
            }
            //alert('Selected files: ' + files.length);
        });
        uploader.bind('UploadProgress', function (up, file) {
            var item = jQuery('#media-item-' + file.id);

            jQuery('.bar', item).width( (200 * file.loaded) / file.size );
            jQuery('.percent', item).html( file.percent + '%' );
        });
        uploader.bind('QueueChanged', function(up) {
            //alert('Queued files: ' + uploader.files.length);
            up.start();
        });
         
        uploader.init();
        
        uploaders.push(uploader);
    });    
});
</script>
