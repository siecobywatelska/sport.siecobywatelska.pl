<?php
$_part = $title[2]*1;  //Part 1 A
//var_dump($_part);
    wp_enqueue_script('plupload-handlers');
    $pageData = $post;
    $pageMeta = get_post_meta($pageData->ID);
    
//    var_dump($pageData);
//    var_dump($pageMeta);

?>
<article id="part_<?php echo $_part; ?>_A" class="partAB textleft">
    <header>
        <div>
<!--            <label>Ankiety</label>-->
            <div id="ankiety">
                <ul class="ankiety">
                    <?php
                        $searchFor = array();
                        $searchFor2 = array();
                        if (!is_array($pageMeta['ankiety'])) $pageMeta['ankiety'] = array($pageMeta['ankiety']);
                        foreach ($pageMeta['ankiety'] as $doc) {
                            $searchFor[] = 'Ankieta:' . $gmina->nazwa_gminy .':'.($doc * 1);
                            $searchFor2[] = 'Ankieta:' . $gmina->nazwa_gminy .':'.($doc * 1 + 1);
                        } 
                        
                        $args = array(
                            'post_type' => 'gmina_ankieta',
                            'posts_per_page' => -1,
    //                        'menu_order' => $_part,
                        );
                        
                        $ankiety = new WP_Query($args);
                        if ($ankiety->have_posts())
                        {
                            $temp = $ankiety->get_posts();
                            $ankiety = array();
                            foreach ($temp as $key => $ankieta)
                            {
                                if (in_array($ankieta->post_title, $searchFor))
                                    $ankiety[$ankieta->post_title] = $ankieta;
                            }
                            foreach ($temp as $key => $ankieta)
                            {
                                if (in_array($ankieta->post_title, $searchFor2))
                                {
                                    $e = explode(':', $ankieta->post_title);
                                    --$e[2];
                                    $titleId = $e[2];
                                    $e = implode(':', $e);
                                    $ankiety[$e]->_ = $ankieta;
                                    $ankiety[$e]->_title = $ankietyTitles[$titleId];
                                }
                            }
                        }
                        else
                            $ankiety = array();
                            
                        sort($ankiety);
                            
                        foreach ($ankiety as $ankieta)
                        {
                            ?>
                                <li class="ankieta"><h5><?php echo $ankieta->_title; ?></h5><?php echo stripslashes($ankieta->post_content); ?><hr class="divider"/><?php echo isset($ankieta->_) ? stripslashes($ankieta->_->post_content) : '-=Brak części drugiej ankiety=-'; ?></li>
                            <?php
                        }
                    ?>
                </ul>
            </div>
            <style>
                #ankiety { width: 1000px; overflow: hidden; margin: 0 auto;}
                ul.ankiety
                {
                    overflow: hidden;
                    height: 310px;
                    width: 100000px;
                }
                
                ul.ankiety li.ankieta
                {
                    overflow: hidden;
                    overflow-y: visible;
                    width: 800px;
                    height: 300px;
                    padding: 5px 90px;
                    float: left;
                }
                
                .podsumowanie_controller
                {
                    width: 100%;
                    height: 15px;
                    position: relative;
                }
                
                .podsumowanie_controller .left
                {
                    float: left;
                    width: 100px;
                }
                
                .podsumowanie_controller .center ul {
                    width: 50%;
                    margin:0 auto;
                }
                
                .podsumowanie_controller .center li {
                    float: left;
                    padding: 0 5px;
                }
                .podsumowanie_controller .center
                {
                    position: relative;
/*                    margin: 0 auto;*/
                    text-align: center;
                    width: 100%;
                }
                
                .podsumowanie_controller .right
                {
                    float: right;
                    width: 100px;
                }
                
            </style>
            <script>
                var actual_ankieta = 0;
                function ankieta_mover() {
                    console.log(actual_ankieta);
                    var elem_width = jQuery('#ankiety li.ankieta').width() + 200; //200 padding from both sides
                    jQuery('#ankiety ul.ankiety').animate({'margin-left': -actual_ankieta*elem_width + 'px'});
                    var elems = jQuery('.podsumowanie_controller li.dot');
                    jQuery(elems).removeClass('active');
                    jQuery(elems[actual_ankieta]).addClass('active');
                }
                
                jQuery(document).ready(function () {
                    jQuery('.podsumowanie_controller .center ul').css('width', jQuery('.podsumowanie_controller .center li').length * (jQuery('.podsumowanie_controller .center li').width() + 2*5) + 'px');
                    
                    jQuery('.podsumowanie_controller .left').click(function () {
                        if (--actual_ankieta < 0) actual_ankieta = 0;
                        ankieta_mover();
                    });
                    
                    jQuery('.podsumowanie_controller .right').click(function () {
                        if (++actual_ankieta > jQuery('.podsumowanie_controller li').length-1) actual_ankieta = jQuery('.podsumowanie_controller li').length -1;
                        ankieta_mover();
                    });
                    
                    jQuery('.podsumowanie_controller li.dot').click(function () {
                        actual_ankieta = jQuery(this).attr('attr');
                        ankieta_mover();
                    });
                    
                });
            </script>
        </div>
        <div class="podsumowanie_controller">
            <div class="left">Poprzednia</div>
            <div class="right">Następna</div>
            
            <div class="center">
                <ul>
                    <?php
                    $i = 0;
                        foreach ($ankiety as $ankieta)
                            echo '<li style="float: left;"attr="'.$i++.'"class="dot '.($i == 1 ? 'active' : '').'">.</li>';
                    ?>
                </ul>
            </div> 
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </header>
    <section class="file_upload_holder clear">
        <header>
<?php
//    writeUploaderHTMLforPart($_part);
?>
        </header>
        <div class="clear page_content">
            <?php
                    $content = apply_filters( 'the_content', $pageData->post_content );
                    $content = str_replace( ']]>', ']]&gt;', $content );   
                    echo $content;
            ?>
        </div>
    </section>
    <div class="clear"></div>
<?php
    writeGminaAnkietaEditor($_part);
?>
    
</article>
<div class="clear"></div>
<a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013' ) )); ?>">Powrót do strony z ankietami</a>