<?php
$ankietyTitles = array(
    1 => 'Cele gminy w zakresie sportu',
    3 => 'Zajęcia sportowe dla mieszkańców',
    5 => 'Dostęp mieszkańców do obiektów sportowych',
    7 => 'Informowanie mieszkańców o ofercie sportowej',
    9 => 'Inne aspekty sportowej polityki gminy',
    
    21 => 'Badanie potrzeb i konsultacje społeczne',
    23 => 'Wspieranie inicjatyw mieszkańców',
);

    get_header();
    $gmina = setiWatchdogUsers::getGmina();

    //Sprawdź uprawenienia
    if (setiWatchdogUsers::getGmina() === false)
    {
        echo 'BRAK UPRAWNIEŃ';
        get_footer();
        return;
    }

    //Sprawdź na którą ankietę wszedł system
    $title = explode(' ',$post->post_title);
    if (count($title) == 2 || count($title) == 3)
    {
        $ankieta = isset($title[2]) ? $title[2]*1 : 0;
        if ($ankieta <= 0)
        { //Lista
            include('miniankieta.php');
            ?>
                <section class="float_left">
                    <h3>Część I - Wpływ sportowej polityki gminy na życie mieszkańców</h3>
                    <ul class="ankieta_part">
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Wstęp cz1' ) )); ?>">Wstęp</a></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 1' ) )); ?>">Cele gminy w zakresie sportu</a></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 3' ) )); ?>">Zajęcia sportowe dla mieszkańców</a></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 5' ) )); ?>">Dostęp mieszkańców do obiektów sportowych</a></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 7' ) )); ?>">Informowanie mieszkańców o ofercie sportowej</a></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 9' ) )); ?>">Inne aspekty sportowej polityki gminy</a></li>
                        <li class="transparent"></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 11' ) )); ?>">Podsumowanie</a></li>
                    </ul>
                </section>
                <section class="float_right">
                    <h3>Część II - Wpływ mieszkańców na politykę sportową gminy</h3>
                    <ul class="ankieta_part">
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Wstęp cz2' ) )); ?>">Wstęp</a></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 21' ) )); ?>">Badanie potrzeb i konsultacje społeczne</a></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 23' ) )); ?>">Wspieranie inicjatyw mieszkańców</a></li>
                        <li class="transparent"></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 31' ) )); ?>">Podsumowanie</a></li>
                        <li class="transparent"></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 101 Notatnik' ) )); ?>">NOTATNIK</a></li>
                        <li><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013 101 Files' ) )); ?>">ZAŁĄCZNIKI</a></li>
                        <li class="transparent"></li>
                        <li><a href="/test.php?command=gmina_archive">Pobierz wszystko</a></li>
                    </ul>
                </section>
                <div class="clear"></div>
            <?php
            get_footer();
            die();
        }
        elseif ( ($ankieta & 1) != 1)
        { //Ankieta zablokowana... Opuść o jeden
            header('Location: '. esc_url(get_permalink( get_page_by_title( $title[0].' '.$title[1].' '.($ankieta-1) ) )));
            die();
        }
    }
/**
* Questionarie 2013 [0] --lista   lista ankiet nieparzystych
* Questionarie 2013 1   --wejśćie (można wejsć.)
* Questionarie 2013 2   --nie da się wejsć (wraca do poprzedniej)
* Questionarie 2013 3
* Questionarie 2013 4   --nie da się wejsć
* .......
* Questionarie 2013 n B
* 
* 
* 
*/
        {

//            include('A.php');
            $pageMeta = get_post_meta($post->ID);
//            var_Dump($pageMeta);
            if (isset($pageMeta['podsumowanie']) && ($pageMeta['podsumowanie'] == true || $pageMeta['podsumowanie'] == 'true' || $pageMEta['podsumowanie']*1 == 1))
                include('podsumowanie.php');
            else
            {
                $dir = get_template_directory() . '/_customs/';
                
                include($dir.'part1.php');
                $pageMeta = get_post_meta($pageData->ID);
                if (!isset($pageMeta['notatnik']))
                {
                    include($dir.'part2.php');
                }
                else
                {
                    echo '<a href="'.esc_url(get_permalink( get_page_by_title( 'Questionarie 2013' ) )).'">Powrót do strony z ankietami</a>';
                }
            }
                include('parts-scripts.php');
        }
        

get_footer();
