<?php
get_header();

$gminy = setiWatchdogUsers::getAllGminy();

if (!function_Exists('setiWatchdogUsers_gminaCheck'))
{
    function setiWatchdogUsers_gminaCheck($gmina)
    {
        $error = false;
        $dupa = array();
        foreach($gmina as $key => $value)
        {
            if (in_array($key,array('kategoria','klub')))
            {
                foreach ($value as $obj)
                {
                    if ($obj['name'] == '' OR $obj['ilosc'] == '' OR $obj['ilosc'] == 0)
                    {
                        $error = true;
                        $dupa[$key][] = $obj;
                    }
                }
            }
            elseif ($key == 'person')
            {
                foreach ($value as $obj)
                    if ($obj == '')
                    {
                        $error = true;
                        $dupa['person'][] = $obj;
                    }
            }
            elseif ($key == 'isDeletes') continue;
            else
            {
                if ($value == '' OR $value == null)
                {
                    $error = true;
                    $dupa['single'][$value] = $key;
                }
            }
        }
        return $error;
    }
}


$raport_array = Array();
                    $args = array(
                        'post_type' => 'gmina_file',
                        'posts_per_page' => -1,
//                        'post_title' => $gmina['nazwa_gminy'],
                        'menu_order' => 999,
                        'post_status' => 'any',
                    );
                    $files = new WP_Query( $args );

                    if ( $files -> have_posts() )
                    {
                        foreach ($files->posts as $file)
                        {
                            if (is_string($file->post_content))
                               $raport_array[$file->post_title] = $file->post_content;
                            //   $raport_array[] = $file->post_content;
                        }
                    }
//echo '<pre>';
//var_dump($files);
//echo '</pre>';
//var_dump($raport_array);

    ?>
        <ul id="gminyMetryczki">
            <?php
                foreach ($gminy as $gmina)
                {
                    if (in_array($gmina['nazwa_gminy'], array('Nowa_Gmina','Szubin'))) continue;
                    if (in_array($gmina['nazwa_gminy'], array('Branszczyk','Debica', 'Duszniki', 'Lidzbark_Warminski'))) continue;
                    if (in_array($gmina['nazwa_gminy'], array('Gdansk','Kamien_Pomorski'))) continue;

                    $active = !setiWatchdogUsers_gminaCheck($gmina);
                    
                    $activeRaport = false;
//                    echo 'Nazwa: ' . $gmina['nazwa_gminy'].' => '.$raport_array[$gmina['nazwa_gminy']].'<br>';
                    if ($raport_array[$gmina['nazwa_gminy']]) $activeRaport = true;
                    ?>
                        <li attr="<?php echo $gmina['nazwa_gminy']; ?>" class="item">
                            <h2 style="width:500px;" class="<?php echo $active ? 'bold' : ''; ?>">
                                <?php echo str_replace('_', ' ',$gmina['nazwa_gminy']); ?>
                                <?php 
                                    echo $activeRaport ? '<span class="clickNarzedzie"><a href="/raport.php?gmina='.$gmina['nazwa_gminy'].'&ankieta=0">Pokaż Narzędzie</a></span>' : '';
                                if ($active)
                                {
                                    $style = $activeRaport ? 'cursor: pointer; color: white;' : ' color: black;';
                                    $val = $activeRaport ? '<a style="color: white; text-decoration: none;" href="/raport.php?gmina='.$gmina['nazwa_gminy'].'">Pobierz raport</a>' : 'Raport niedostepny';
                                    echo '<span class="clickRaport" style="'.($style).'">'.($val).'</span>';
                                }
                                ?>
                                <?php echo $active ? '<span class="clickMetryczka">Pokaż metryczkę</span>' : ''; ?>
                            </h2>
                            <?php
                                if ($active)
                                {
                                    ?>
                                        <div class="hidden" id="metryczka-<?php echo $gmina['nazwa_gminy']; ?>">
                                            <div id="miniankieta">
                                                <form method="post" class="dissabled">
                                                    <section id="gmina_base" style="width:45%; float: left;">

                                                        <div>
                                                            <label>Nazwa gminy</label><input disabled="disabled" type="text" name="gmina[nazwa]" value="<?php echo $gmina['nazwa']; ?>" />
                                                        </div>
                                                        <div>
                                                            <label>Typ gminy</label>
                                                            <select name="gmina[typ]" disabled="disabled">
                                                                <option value="">-- Wybierz Typ ---</option>
                                                                <option <?php echo $gmina['typ'] == 'miejska' ? 'selected="selected"' : ''; ?> value="miejska">Miejska</option>
                                                                <option <?php echo $gmina['typ'] == 'wiejska' ? 'selected="selected"' : ''; ?> value="wiejska">Wiejska</option>
                                                                <option <?php echo $gmina['typ'] == 'mw' ? 'selected="selected"' : ''; ?> value="mw">Miejsko-Wiejska</option>
                                                            </select>
                                                        </div>
                                                        <div>
                                                            <label>Liczba mieszkańców gminy</label><input type="text" disabled="disabled" name="gmina[mieszkancy]" value="<?php echo number_format($gmina['mieszkancy'],0); ?>" />
                                                        </div>

                                                <!-- BEGIN: From 2013.10.02 -->        
                                                        <div>
                                                            <label>Kwota przeznaczona z działu <em>926</em> budżetu monitorowanej gminy na sport w badanym roku</label>
                                                            <input type="text" disabled="disabled" name="gmina[kwota]" value="<?php echo number_format($gmina['kwota'], 0); ?>" />
                                                        </div>
                                                        
                                                        <div style="clear: both;">
                                                            <label>Procentowy udział wydatków gminy z działu <em>926</em> na sport w całorocznym budżecie</label>
                                                            <input type="text" disabled="disabled" name="gmina[procenty]" value="<?php echo $gmina['procenty']; ?>"/>
                                                        </div>
                                                        
                                                <!-- END: From 2013.10.02 -->        
                                                    </section>
                                                    <section style="width:45%; float: right;">
                                                        <div form_type="person">
                                                           <label>Osoby monitorujące gminę</label>
                                                           <ul>
                                                               <?php
                                                   //            var_dump($gmina);
                                                                   if (count($gmina['person']) == 0) { $gmina['person'] = array(''); }
                                                                   foreach ($gmina['person'] as $value)
                                                                   {
                                                                       ?>
                                                                           <li>
                                                                               <label>Osoba</label>
                                                                               <div class="person"><input type="" disabled="disabled" name="gmina[person][]" value="<?php echo $value; ?>"/></div>
                                                                               <div class="clear"></div>
                                                                           </li>
                                                                       <?php
                                                                   }
                                                               ?>
                                                           </ul>
                                                           <div class="clear"></div>
                                                       </div>   
                                                       <div form_type="kategoria">
                                                           <label>Liczba obiektów sportowych należących do monitorowanej gminy (z podziałem na kategorie np.: boiska do piłki nożnej, baseny, korty tenisowe, etc.)</label><div style="display:none;" class="info">Nie wprowadzać dla ilości równej zero</div>
                                                           <ul>
                                                               <?php
                                                                   if (count($gmina['kategoria']) == 0) { $gmina['kategoria'] = array(''=>''); }
                                                                   foreach ($gmina['kategoria'] as $cat )
                                                                   {
                                                                       $nazwa = $cat['name'];
                                                                       $ilosc = $cat['ilosc'];
                                                                       ?>
                                                                           <li style="border-bottom: 1px grey dotted;">
                                                                               <div style="width: 270px;">
                                                                                   <label style="line-height: 17px;">Kategoria</label>
                                                                                   <p class="dissabled"><?php echo $nazwa; ?></p>
                                                                               </div>
                                                                               <div class="clear" style="width: 270px;float: left;">
                                                                                   <label style="line-height: 17px;">Liczba obiektów</label>
                                                                                   <p class="dissabled"><?php echo $ilosc; ?></p>
                                                                               </div>
                                                                               <div class="clear"></div>
                                                                           </li>
                                                                       <?php
                                                                   }
                                                               ?>
                                                           </ul>
                                                           <div class="clear"></div>
                                                       </div>
                                                       <div form_type="klub">
                                                           <label>Liczba klubów sportowych finansowanych przez monitorowaną gminę (z podziałem na kategorie np. klub piłki nożnej, klub siatkówki, klub szermierczy etc.)</label><div style="display:none;" class="info">Nie wprowadzać dla ilości równej zero</div>
                                                           <ul>
                                                               <?php
                                                                   if (count($gmina['klub']) == 0) { $gmina['klub'] = array(''=>''); }
                                                                   foreach ($gmina['klub'] as $cat )
                                                                   {
                                                                       $nazwa = $cat['name'];
                                                                       $ilosc = $cat['ilosc'];
                                                                       ?>
                                                                           <li style="border-bottom: 1px grey dotted;">
                                                                               <div style="width: 270px;">
                                                                                   <label style="line-height: 17px;">Kategoria</label>
                                                                                   <p class="dissabled"><?php echo $nazwa; ?></p>
                                                                               </div>
                                                                               <div class="clear" style="width: 270px;float: left;">
                                                                                   <label style="line-height: 17px;">Liczba klubów</label>
                                                                                   <p class="dissabled"><?php echo $ilosc; ?></p>
                                                                               </div>
                                                                               <div class="clear"></div>
                                                                           </li>
                                                                       <?php
                                                                   }
                                                               ?>
                                                           </ul>
                                                           <div class="clear"></div>
                                                       </div>
                                                    </section>   
                                                </form>
                                            </div>
                                        </div>
                                    <?php
                                }
                            ?>
                        </li>
                    <?php
                    
                }
            ?>
        </ul><div class="clear"></div>
        <script>
            jQuery(document).ready( function () {
                jQuery("h2.bold span.clickMetryczka").click( function () {
                    var gmina = jQuery(this).parent().parent().attr('attr');
                    console.log(this);
                    console.log(gmina);
                    if (jQuery(this).hasClass('shown')) {
                        jQuery(this).removeClass('shown').html('Pokaż metryczkę');
                        jQuery("#metryczka-" + gmina).fadeOut(1000);
                    } else {
                        jQuery(this).addClass('shown').html('Schowaj metryczkę');
                        jQuery("#metryczka-" + gmina).fadeIn(1000);
                    }
                })
            });
        </script>
        <style>
        #gminyMetryczki .item h2 span.clickNarzedzie { background-color: orange; }
        #gminyMetryczki section#gmina_base>div {padding-left: 30px;}
            #gminyMetryczki li.item div.hidden .left { float: left; }
            #gminyMetryczki li.item div.hidden > div
            {
                float: left;
            }
            #gminyMetryczki li.item h2.bold { color: black; }
            #gminyMetryczki li.item .hidden
            { 
                float: left;
                width: 450px;
            }
            #gminyMetryczki li.item h2
            {
                font-weight: normal;
                color: gray;
float: left;
width: 200px;
            }
            
            #gminyMetryczki li.item
            {
/*                width: 400px;*/
                float: left;
                clear: both;
            }
        </style>
<?php
    get_footer();