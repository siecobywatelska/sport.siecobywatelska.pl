<?php
    wp_enqueue_script('plupload-handlers');
$gmina = array();
    if (isset($_POST))
    { //Try to save it

        
        //Znajdź teraz tę gminę w bazie...
        if (setiWatchdogUsers::getGmina() !== false)
        {
            echo '<h2 class="center">Metryczka</h2>';
            $saved = setiWatchdogUsers::saveGminaFromFORM($_POST['gmina']);
            if ($saved['base'] && $saved['kats'] && $saved['pers'] && $saved['klubs'])
                echo '<div class="saved">Odpowiedzi został zapisane</div>';
            $gmina = setiWatchdogUsers::getGminaData(true);
        }
        else
        {
            header("HTTP/1.0 403 No permission to enter the site");
            ?>
                <h2>Brak uprawnień!</h2>
            <?php
            return;
        }
        
    }    
    

if (!function_Exists('setiWatchdogUsers_gminaCheck'))
{
    function setiWatchdogUsers_gminaCheck($gmina)
    {
        $error = false;
        $dupa = array();
        foreach($gmina as $key => $value)
        {
            if (in_array($key,array('kategoria','klub')))
            {
                foreach ($value as $obj)
                {
                    if ($obj['name'] == '' OR $obj['ilosc'] == '' OR $obj['ilosc'] == 0)
                    {
                        $error = true;
                        $dupa[$key][] = $obj;
                    }
                }
            }
            elseif ($key == 'person')
            {
                foreach ($value as $obj)
                    if ($obj == '')
                    {
                        $error = true;
                        $dupa['person'][] = $obj;
                    }
            }
            elseif ($key == 'isDeletes') continue;
            else
            {
                if ($value == '' OR $value == null)
                {
                    $error = true;
                    $dupa['single'][$value] = $key;
                }
            }
        }
        return $error;
    }
}
?>
<div id="miniankieta">
<form method="post">
    <section id="gmina_base" style="width:45%; float: left;">

        <div>
            <label>Nazwa gminy</label><input type="text" name="gmina[nazwa]" value="<?php echo $gmina['nazwa']; ?>" />
        </div>
        <div>
            <label>Typ gminy</label>
            <select name="gmina[typ]">
                <option value="">-- Wybierz Typ ---</option>
                <option <?php echo $gmina['typ'] == 'miejska' ? 'selected="selected"' : ''; ?> value="miejska">Miejska</option>
                <option <?php echo $gmina['typ'] == 'wiejska' ? 'selected="selected"' : ''; ?> value="wiejska">Wiejska</option>
                <option <?php echo $gmina['typ'] == 'mw' ? 'selected="selected"' : ''; ?> value="mw">Miejsko-Wiejska</option>
            </select>
        </div>
        <div>
            <label>Liczba mieszkańców gminy</label><input type="text" name="gmina[mieszkancy]" value="<?php echo $gmina['mieszkancy']; ?>" />
        </div>

<!-- BEGIN: From 2013.10.02 -->        
        <div>
            <label>Kwota przeznaczona z działu <em>926</em> budżetu monitorowanej gminy na sport w badanym roku</label>
            <input type="text" name="gmina[kwota]" value="<?php echo $gmina['kwota']; ?>" />
        </div>
        
        <div style="clear: both;">
            <label>Procentowy udział wydatków gminy z działu <em>926</em> na sport w całorocznym budżecie</label>
            <input type="text" name="gmina[procenty]" value="<?php echo $gmina['procenty']; ?>"/>
        </div>
        
        <div class="publikowanie">
            <label>Metryczka Gminy Jest</label>
            <?php $error = setiWatchdogUsers_gminaCheck($gmina); ?>
            <div style="color: <?php echo $error ? 'red' : 'green'; ?>"><?php echo $error ? 'Nieopublikowana' : 'Opublikowana'; ?></div>
        </div>
<!-- END: From 2013.10.02 -->
<!-- BEGIN: From 2014-07-01 -->
    <article class="partAB">
        <style>#plupload-browse-button999 {width: 100px !important;}</style> 
        <header>
            Poniżej proszę wgrać JEDEN plik pdf, który będzie można ściągnąć ze strony głównej po wybraniu opcji: ,,Pobierz raport''.
        </header>
        <section class="file_upload_holder">
            <header>
                <?php
                    writeUploaderHTMLforPart(999);
                ?>
                <div class="clear"></div>
            </header>
        </section>
        <?php include('parts-scripts.php');?>
    </article>
<!-- END: From 2014-07-01 -->        
    </section>
    <section style="width:45%; float: right;">
        <div form_type="person">
           <label>Osoby monitorujące gminę</label>
           <ul>
               <?php
   //            var_dump($gmina);
                   if (count($gmina['person']) == 0) { $gmina['person'] = array(''); }
                   foreach ($gmina['person'] as $value)
                   {
                       ?>
                           <li>
                               <label>Osoba</label>
                               <div class="person"><input type="" name="gmina[person][]" value="<?php echo $value; ?>"/></div>
                               <div class="controller">
                                   <div class="remove">Usuń osobę</div>
                               </div>
                               <div class="clear"></div>
                           </li>
                       <?php
                   }
               ?>
           </ul>
           <div class="controller">
               <div class="add">Dodaj kolejną osobę</div>
           </div>
           <div class="clear"></div>
       </div>   
       <div form_type="kategoria">
           <label>Liczba obiektów sportowych należących do monitorowanej gminy (z podziałem na kategorie np.: boiska do piłki nożnej, baseny, korty tenisowe, etc.)</label><div style="display:none;" class="info">Nie wprowadzać dla ilości równej zero</div>
           <ul>
               <?php
                   if (count($gmina['kategoria']) == 0) { $gmina['kategoria'] = array(''=>''); }
                   foreach ($gmina['kategoria'] as $cat )
                   {
                       $nazwa = $cat['name'];
                       $ilosc = $cat['ilosc'];
                       ?>
                           <li style="border-bottom: 1px grey dotted;">
                               <div style="width: 270px;">
                                   <label>Kategoria</label>
                                   <input type="" name="gmina[kategoria][nazwa][]" value="<?php echo $nazwa; ?>" />
                               </div>
                               <div class="clear" style="width: 270px;float: left;">
                                   <label>Liczba obiektów</label>
                                   <input type="" name="gmina[kategoria][ilosc][]" value="<?php echo $ilosc; ?>" />
                               </div>
                               <div class="controller">
                                   <div class="remove">Usuń kategorię</div>
                               </div>
                               <div class="clear"></div>
                           </li>
                       <?php
                   }
               ?>
           </ul>
           <div class="controller">
               <div class="add">Dodaj kolejną kategorię</div>
           </div>
           <div class="clear"></div>
       </div>
       <div form_type="klub">
           <label>Liczba klubów sportowych finansowanych przez monitorowaną gminę (z podziałem na kategorie np. klub piłki nożnej, klub siatkówki, klub szermierczy etc.)</label><div style="display:none;" class="info">Nie wprowadzać dla ilości równej zero</div>
           <ul>
               <?php
                   if (count($gmina['klub']) == 0) { $gmina['klub'] = array(''=>''); }
                   foreach ($gmina['klub'] as $cat )
                   {
                       $nazwa = $cat['name'];
                       $ilosc = $cat['ilosc'];
                       ?>
                           <li style="border-bottom: 1px grey dotted;">
                               <div style="width: 270px;">
                                   <label>Kategoria</label>
                                   <input type="" name="gmina[klub][nazwa][]" value="<?php echo $nazwa; ?>" />
                               </div>
                               <div class="clear" style="width: 270px;float: left;">
                                   <label>Liczba klubów</label>
                                   <input type="" name="gmina[klub][ilosc][]" value="<?php echo $ilosc; ?>" />
                               </div>
                               <div class="controller">
                                   <div class="remove">Usuń klub</div>
                               </div>
                               <div class="clear"></div>
                           </li>
                       <?php
                   }
               ?>
           </ul>
           <div class="controller">
               <div class="add">Dodaj kolejną kategorię klubu</div>
           </div>
           <div class="clear"></div>
       </div>
    </section>    
    
    <div class="clear" style="text-align: center; margin-bottom: 10px; "><input type="submit" value="Zapisz" style="width: 200px; font-weight: bold; padding: 3px;"/></div>
</form>
</div>
<script>
jQuery(document).ready(function () {
    jQuery('.controller .remove').addClass('pointer');
    
    //Usuwaczki
    jQuery(document).on('click', 'div[form_type="person"] .controller .remove', function (e,o) {
        if (jQuery('div[form_type="person"] li').length > 1) {
            jQuery(this).parent().parent().remove();
        }
    });
    
    jQuery(document).on('click', 'div[form_type="klub"] .controller .remove', function (e,o) {
        if (jQuery('div[form_type="klub"] li').length > 1) {
            jQuery(this).parent().parent().remove();
        }
    });

    jQuery(document).on('click', 'div[form_type="kategoria"] .controller .remove', function (e,o) {
        if (jQuery('div[form_type="kategoria"] li').length > 1) {
            jQuery(this).parent().parent().remove();
        }
    });
    
    jQuery('div[form_type="person"] .controller .add').click( function (e,o) {
        var li = '<?php echo str_replace(array("\n","\r"),'',('                        <li>
                            <label>Osoba</label>
                            <div class="person"><input type="" name="gmina[person][]" value=""/></div>
                            <div class="controller">
                                <div class="remove pointer">Usuń osobę</div>
                            </div>
                        </li>')); ?>';
        jQuery('div[form_type="person"] ul').append(li);
    }).addClass('pointer');
    jQuery('div[form_type="kategoria"] .controller .add').click( function (e,o) {
        var li = '<?php echo str_replace(array("\n","\r"),'',('                        <li style="border-bottom: 1px grey dotted;">
                            <div>
                                <label>Kategoria</label>
                                <input type="" name="gmina[kategoria][nazwa][]" value="" />
                            </div>
                            <div class="clear" style="width: 270px;float: left;">
                                <label>Liczba obiektów</label>
                                <input type="" name="gmina[kategoria][ilosc][]" value="" />
                            </div>
                            <div class="controller">
                                <div class="remove pointer">Usuń kategorię</div>
                            </div>
                            <div class="clear"></div>
                        </li>')); ?>';
        jQuery('div[form_type="kategoria"] ul').append(li);
    }).addClass('pointer');
    
    jQuery('div[form_type="klub"] .controller .add').click( function (e,o) {
        var li = '<?php echo str_replace(array("\n","\r"),'',('                        <li style="border-bottom: 1px grey dotted;">
                            <div>
                                <label>Kategoria</label>
                                <input type="" name="gmina[klub][nazwa][]" value="" />
                            </div>
                            <div class="clear" style="width: 270px;float: left;">
                                <label>Liczba klubów</label>
                                <input type="" name="gmina[klub][ilosc][]" value="" />
                            </div>
                            <div class="controller">
                                <div class="remove pointer">Usuń kategorię</div>
                            </div>
                            <div class="clear"></div>
                        </li>')); ?>';
        jQuery('div[form_type="klub"] ul').append(li);
    }).addClass('pointer');

});
</script>