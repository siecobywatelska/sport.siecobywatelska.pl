<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 */
ob_start();
wp_enqueue_script( 'jquery' );
wp_enqueue_script( 'cufon', 'http://cdnjs.cloudflare.com/ajax/libs/cufon/1.09i/cufon-yui.js' );
wp_enqueue_script( 'font', get_template_directory_uri().'/Clio/ClioFont.js' );
global $current_user;
get_currentuserinfo();
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
        <meta name="keywords" content="<?php echo get_theme_mod( 'setKey_cont', 'Please fill keywords.' ); ?>">
	<title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/reset.css" rel="stylesheet" media="all" />
    <link type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css" rel="stylesheet" media="all" />
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
        <?php custom_settings(); ?>
	<?php wp_head(); ?>
    <!--[if gte IE 9]>
      <style type="text/css">
        .gradient {
           filter: none;
        }
      </style>
    <![endif]-->    
</head>


<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
            <div id="logoImg">
			    <a class="home-link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                                    <img style="max-height: 90px;" src="<?php echo get_template_directory_uri();?>/img/logoOMS2013.JPG"/>
    <!--				<h1 class="site-title"><?php // bloginfo( 'name' ); ?></h1>
				    <h2 class="site-description"><?php // bloginfo( 'description' ); ?></h2>-->
			    </a>
            </div>

			<div id="navbar" class="navbar">
				<nav id="site-navigation" class="navigation main-navigation" role="navigation">
					<?php // wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
                                    <?php  
                                        if (is_user_logged_in())
                                        {
//                                            jeśli użytkownik jest zalogowany
                                           echo wp_nav_menu(array('menu' => 'zalogowany' )); 
                                         }
                                         else
                                        {
//                                             jeśli użytkownik nie jest zalogowany
                                            echo wp_nav_menu(array('menu' =>'niezalogowany'));
                                         };
                                     ?>
					<?php  ?>
					<?php //get_search_form(); ?>
                    <div class="clear"></div>
				</nav><!-- #site-navigation -->
                <div class="clear"></div>
			</div><!-- #navbar -->
		</header><!-- #masthead -->

		<div id="main" class="site-main clear">
