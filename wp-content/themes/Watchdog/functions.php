<?php
function pre_posts_category($qry) {
  if (is_category() && is_main_query()) {
    // no idea what conditions you want, but below is a sample
    $page = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $qry->set('posts_per_page',5);
    $qry->set('paged', $page);
    $qry->set('ORDER', 'DESC');
  }
}
add_action('pre_get_posts','pre_posts_category');
  
register_nav_menu( 'primary', __( 'Navigation Menu', 'default' ) );
function arphabet_widgets_init() {

  register_sidebar( array(
    'name' => 'Home right sidebar',
    'id' => 'home_right_1',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h2 class="rounded">',
    'after_title' => '</h2>',
  ) );
}
add_action( 'widgets_init', 'arphabet_widgets_init' );
$defaults = array(
  'default-image'          => '',
  'random-default'         => false,
  'width'                  => 0,
  'height'                 => 0,
  'flex-height'            => false,
  'flex-width'             => false,
  'default-text-color'     => '',
  'header-text'            => true,
  'uploads'                => true,
  'wp-head-callback'       => '',
  'admin-head-callback'    => '',
  'admin-preview-callback' => '',
);
add_theme_support( 'custom-header', $defaults );
add_theme_support( 'automatic-feed-links' );
add_action( 'customize_register', 'hg_customize_register' );
function custom_settings(){
    require_once('custom_settings.php');
}

function hg_customize_register($wp_customize)
{
  $colors = array();
  $colors[] = array( 'slug'=>'content_bg_color', 'default' => '#ffffff', 'label' => __( 'Content Background Color', 'YOUR_THEME_NAME' ) );
  $colors[] = array( 'slug'=>'content_text_color', 'default' => '#000000', 'label' => __( 'Content Text Color', 'YOUR_THEME_NAME' ) );
  $colors[] = array( 'slug'=>'content_link_color', 'default' => '#000000', 'label' => __( 'Link color', 'YOUR_THEME_NAME' ) );
  $colors[] = array( 'slug'=>'content_linkhover_color', 'default' => '#000000', 'label' => __( 'Link hover color', 'YOUR_THEME_NAME' ) );

  foreach($colors as $color)
  {
    // SETTINGS
    $wp_customize->add_setting( $color['slug'], array( 'default' => $color['default'], 'type' => 'option', 'capability' => 'edit_theme_options' ));

    // CONTROLS
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, $color['slug'], array( 'label' => $color['label'], 'section' => 'colors', 'settings' => $color['slug'] )));
  }
}

add_action('customize_register', 'setWidths');
function setWidths($wp_customize) {
 
    $wp_customize->add_section( 'setWidths_settings', array(
        'title'          => 'Widths',
        'priority'       => 30,
    ) );
 
    $wp_customize->add_setting( 'page_width', array(
        'default'        => 'default_value',
    ) );
 
    $wp_customize->add_control( 'page_width', array(
        'label'   => 'Page width',
        'section' => 'setWidths_settings',
        'type'    => 'text',
    ) );


 
}
 function truncate($string, $length = 80, $etc = '...', $break_words = true, $middle = false) {
        if ($length == 0)
            return '';
 
        if (strlen($string) > $length) {
            $length -= min($length, strlen($etc));
            if (!$break_words && !$middle) {
                $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length + 1));
            }
            if (!$middle) {
                return substr($string, 0, $length) . $etc;
            } else {
                return substr($string, 0, $length / 2) . $etc . substr($string, -$length / 2);
            }
        } else {
            return $string;
        }
    }

function writeGminaAnkietaEditor($_part)
{
    ?>
        <section class="editor" attr="<?php echo $_part; ?>">
            <header>
                <?php
                    $tresc = setiWatchdogUsers::getGminaAnkieta($_part);
                    echo wp_editor(stripslashes($tresc->post_content),"editor_{$_part}", array(
                        'media_buttons'=> false,
                        'textarea_name'=> "input_{$_part}",
            //            'quicktags'=> false,
                        'tinymce' => array(
                                    'theme_advanced_buttons1' => 'undo,redo,|,formatselect,|,bold,italic,underline,|,' .
                                    'bullist,numlist,|,outdent,indent,|,blockquote,|,justifyleft,justifycenter' .
                                    ',justifyright,justifyfull,|,link,unlink,|' .
                                    ',spellchecker,forecolor,|,hr,removeformat,visualaid,separator,sub,sup,separator,charmap',
                                    'theme_advanced_buttons2' => '',
                        ),
                    ));
                ?>
                <div id="editor_static_<?php echo $_part; ?>" class="editor_static"></div>
            </header>
            <div class="editor_buttons">
                <input class="save" type="button" attr="<?php echo $_part; ?>" value="Zapisz" />
                <div class="message">&nbsp;</div>
            </div>
        </section>
    <?php
}
function writeUploaderHTMLforPart($_part)
{
    ?>
            <div id="uploader" class="uploader" attr="<?php echo $_part; ?>">
                <div id="drag-drop-area<?php echo $_part; ?>" class="drag-drop-area">
                    <div class="drag-drop-inside">
                        <p class="drag-drop-info"><?php _e('Upuść pliki tutaj'); ?></p>
                        <p><?php _ex('LUB', 'Uploader: Upuść pliki tutaj - LUB - Wybierz pliki'); ?></p>
                        <p class="drag-drop-buttons"><input id="plupload-browse-button<?php echo $_part; ?>" type="button" value="<?php esc_attr_e('Wybierz pliki'); ?>" class="button" /></p>
                    </div>
                </div>
            </div>
            <div id="media-items<?php echo $_part; ?>" class="uploader_list">    
                <?php
                    echo '<div class="media-item child-of-0" style="margin-top: -15px; line-height: 15px; font-weight: bold;">Lista plików</div>';
                    $gmina = setiWatchdogUsers::getGmina();
                    $args = array(
                        'post_type' => 'gmina_file',
                        'posts_per_page' => -1,
                        'post_title' => $gmina->nazwa_gminy,
                        'menu_order' => $_part,
                    );
                    $files = new WP_Query( $args );
                    if ( $files -> have_posts() )
                    {
                        $files = $files->get_posts();
                        
                        
            //            while ( $posts -> have_posts() )            
                        foreach ( $files as $file )            
                        {
        //                    isset($i) ? ++$i : $i = 0;
                            ?>
                                    <div class="media-item child-of-0" id="media-item-<?php echo $file->ID; ?>">
                                        <div class="progress" style="display: none;">
                                            <div class="percent">100%</div><div class="bar" style="width: 200px;"></div>
                                        </div>
                                        <div class="filename original" attr="<?php echo $file->ID; ?>"><a href="<?php echo ( get_permalink( $file->ID ) ); ?>"><?php echo $file->post_content_filtered; ?></a></div>
                                        <div class="remove" menu="<?php echo $_part; ?>" nonce="<?php echo wp_create_nonce('gmina-del-attachment-'.$file->ID); ?>" attr="<?php echo $file->ID; ?>">Usuń</div>
                                    </div>
                            <?php
                        }
                    }
                    wp_reset_query();
                ?>
            </div>
    <?php
}