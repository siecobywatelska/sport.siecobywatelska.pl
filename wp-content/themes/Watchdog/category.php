<?php get_header();?>
<section id="primary" class="site-content">
    <div id="content" role="main">

    <?php if ( have_posts() ) : ?>
      <?php
      /* Start the Loop */
      while ( have_posts() ) : the_post();?>
                    <div class="category_post">
                        <h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                        <p class="post_category_content"><?php the_content(); ?></p>
                    </div>
                    
                    
      <?php endwhile;?>
<?php
    global $wp_query;
   // if( $wpquery->max_num_pages >1){
      $big = 999999999; // need an unlikely integer

      echo paginate_links( array(
     'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
     'format' => '?paged=%#%',
     'current' => max( 1, get_query_var('paged') ),
     'total' => $wp_query->max_num_pages,
  'prev_text'    => __('« Nowsze'),
  'next_text'    => __('Starsze »'),
       ) );
   //}
?>
    <?php else : ?>
      <?php get_template_part( 'content', 'none' ); ?>
    <?php endif; ?>

    </div><!-- #content -->
  </section><!-- #primary -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>