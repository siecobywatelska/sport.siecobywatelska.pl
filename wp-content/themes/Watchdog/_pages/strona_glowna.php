<?php
    get_header();
    $args = array(
        'post_type' => 'page',
        'posts_per_page' => -1,
        'post_parent' => $post->ID,
        'orderby' => 'menu_order ASC',
    );
    $boxes = array();
    {
        $temp = new WP_Query( $args );
        if ( $temp -> have_posts() )
        {
            $temp = $temp->get_posts();
            foreach ( $temp as $tmp )            
                $boxes[] = $tmp;
        }
    }
?>
<div id="start_content">
    <div id="start_content_page" class="float_left">
        <?php
            $box = $boxes[0];
            echo '<h1><a href="'.(get_permalink($box)).'">'.$box->post_title.'</a></h1>';
//            echo trim_content($box->post_content, 500);
            echo $box->post_content;
        ?>
    </div>
    <div id="start_content_news" class="float_right">
        <strong>Aktualności:</strong>
        <?php query_posts( 'cat=2' ); ?>
        <?php if ( have_posts() ) : ?>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();?>
                    <div class="category_post">
                        <h1 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
<!--                        <p class="post_category_content"><?php // the_content(); ?></p>-->
                    </div>
                    
                    
                        <?php endwhile;?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
    </div>
    <div class="clear"></div>
    
    
    
<!--    <div class="boxes">
        <div class="box_content start_right">
        <?php
//            $box = $boxes[0];
//            echo '<h1><a href="'.(get_permalink($box)).'">'.$box->post_title.'</a></h1>';
//            echo trim_content($box->post_content, 500);
        ?>
        </div>
        <div class="box_content start_left start_right">
        <?php
//            $box = $boxes[1];
//            echo '<h1><a href="'.(get_permalink($box)).'">'.$box->post_title.'</a></h1>';
//            echo trim_content($box->post_content, 260);
        ?>
        </div>
        <div class="box_content start_left">
        <?php
//            $box = $boxes[2];
//            echo '<h1><a href="'.(get_permalink($box)).'">'.$box->post_title.'</a></h1>';
//            echo trim_content($box->post_content, 260);
        ?>
        </div>
    </div>
    <div class="boxes">
        <div class="box_content start_right">
            to jest box_content
        </div>
        <div class="box_content start_left start_right">
            to jest box_content
        </div>
        <div class="box_content start_left">
            to jest box_content
        </div>
    </div>-->
</div>
<?php get_footer(); ?>
