<?php
    if ($post->post_type == 'gmina_file')
    {
        header("Location: " . home_url() . '/front_uploads/'.$post->post_title.'/'.$post->post_content);
        die('file');
    }

    $titles = explode(' ', $post->post_title);
    if (strtolower($titles[0]) == 'questionarie')
    {
        include('_customs/questions.php');
        return;
    }
    elseif (strtolower($titles[0]) == 'files')
    {
        include('_customs/files.php');
        return;
    }
    elseif (strtolower($titles[0]) == 'gminues')
    {
        include('_customs/gminues.php');
        return;
    }
    if (strtolower($post->post_title) == strtolower('Strona Główna'))
    {
        include('_pages/strona_glowna.php');
        return;
    }
?>
<?php get_header();?>
<div id="content" class="site-content" role="main">
    <?php /* The loop */ ?>
    <?php while ( have_posts() ) : the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">
                            <?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
                            <div class="entry-thumbnail">
                                    <?php the_post_thumbnail(); ?>
                            </div>
                            <?php endif; ?>

                            <h1 class="entry-title"><?php the_title(); ?></h1>
                    </header><!-- .entry-header -->

                    <div class="entry-content page_content">
                            <?php the_content(); ?>
                            <?php
                                if ($titles[0] == 'Wstęp' && (in_array($titles[1], array('cz1','cz2'))))
                                {
                                    ?><a href="<?php echo esc_url(get_permalink( get_page_by_title( 'Questionarie 2013' ) )); ?>">Powrót do strony z ankietami</a><?php
                                }
                            ?>
                    </div><!-- .entry-content -->

                    <footer class="entry-meta">
                    </footer><!-- .entry-meta -->
            </article><!-- #post -->
    <?php endwhile; ?>

</div><!-- #content -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>

