<?php 
$content_text_color = get_option('content_text_color'); 
$content_bg_color = get_option('content_bg_color');
$content_link_color = get_option('content_link_color'); 
$content_linkhover_color = get_option('content_linkhover_color');
?>
<style> 
            body{background-color: <?php echo $content_bg_color; ?>;}
            #page { 
                    color:  <?php echo $content_text_color; ?>; 
                    width:  <?php echo get_theme_mod( 'page_width', 'default_value' ); ?>;
            } 
            a { color:  <?php echo $content_link_color; ?>; } 
            a:hover { color:  <?php echo $content_linkhover_color; ?>; } 
</style>