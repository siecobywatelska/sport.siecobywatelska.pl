<?php
/*
Plugin Name: Seti's TinyMCE Settings Modifier
Plugin URI: http://wordpress.chibi.pl/plugins/TinyMCE
Description: Adds new features to default editor on admin page.
Author: Artur (Seti) Łabudziński
Author URI: http://wordpress.chibi.pl/
Version: 1.0
*/

class setiTinyMCEsettings
{
    const VERSION = '1.0';
    
    public function __construct()
    {
        add_filter('tiny_mce_before_init', array($this, 'doTinyButtons' ), 99);
    }
    
    public function doTinyButtons ($in)
    {
        $in['language'] = 'pl';
        $in['theme_advanced_buttons1'] = 'undo,redo,|,formatselect,|,bold,italic,underline,|,' .
        'bullist,numlist,|,outdent,indent,|,blockquote,|,justifyleft,justifycenter' .
        ',justifyright,justifyfull,|,link,unlink,|' .
        ',spellchecker,forecolor,|,hr,removeformat,visualaid,separator,sub,sup,separator,charmap';
        $in['theme_advanced_buttons2'] = '';
        return $in;
    }
  
    //Initiation/installation of the pluging.
    static public function install()
    {
        // Update the database version setting.
        update_option( 'seti_tinymce_settings__db_version', self::VERSION );
        
    }
    
    protected function getDefaultSettings()
    {
        return array (
            //Ver 1.0
        );
    }
    
    public function update()
    {
        // Update the database version setting.
        update_option( 'seti_tinymce_settings__db_version', self::VERSION );

        // Get settings (from db and the default one)
        $settings = get_option( 'seti_tinymce_settings__db_version' );
        $default_settings = self::getDefaultSettings();

        // For each default setting
        foreach ( $default_settings as $key => $val )
        {
            // Merge the setting
            if ( !isset( $settings[$key] ) )
                $settings[$key] = $val;
        }

        // Update
        update_option( 'seti_tinymce_settings__db_version', $settings );
    }
    
}
//Function that just created the object and ignores more...
function initialize_setiTinyMCEsettings()
{
    new setiTinyMCEsettings();
}

add_action('plugins_loaded', 'initialize_setiTinyMCEsettings');
add_action('activate_' . plugin_basename( __FILE__ ), array('setiTinyMCEsettings','install'), 1);
//add_action('admin_init', array('setiAdminLocker','install'), 1);

  