<?php
/*
Plugin Name: Seti's [Watchdog] Upload FileType Restrictions
Plugin URI: http://wordpress.chibi.pl/plugins/watchdog/files
Description: Allows uploading of some additional file extentions.  
Author: Artur (Seti) Łabudziński
Author URI: http://wordpress.chibi.pl/
Version: 1.0
*/

class setiWatchdogFiles
{
    static protected $url;
    const VERSION = '1.0';
    
    public function __construct()
    {
        self::$url = plugin_dir_url(__FILE__);
        add_action('watchdog_files', array($this, 'show_files'));
        add_filter('upload_mimes', array($this,'addUploadMimes'));    
        add_action('admin_menu', array($this,'doAdminMenu'));
    }
    
    public function addUploadMimes($mimes,$user = null) {
        $array = array (
                'pdf|doc|docx|txt' => 'application/octet-stream',
        );   
        $mimes = array_merge($mimes, $array);
         
        return $mimes;
    }
    
    /**
    * Sprawdź czy była już jakaś wcześniej instalacja.
    * 
    */
    static public function install()
    {
        global $wpdb;
        // Update the database version setting.
        update_option( 'seti_watchdog_files__db_version', self::VERSION );
    }
    
    
    static protected function getDefaultSettings()
    {
        return array(
            '' => ''
        );
    }
    
    public function doAdminMenu()
    {
        add_menu_page('Seti\'s Watchdog Pluggins', 'Watchdog', 'administrator', 'Watchdog', array($this, 'parseAdminPageFiles'), self::$url . 'images/seti.gif', 99);
    }
    
    public function parseAdminPageFiles($data)
    {
        ?>
            <section>
                <h2>Watchdog plugin - Files</h2>
                <div>
                    <p>Moduł pozwalający administratorom wysyłanie wielu rodzajów plików. Do tego zawiera moduł odbierania plików dla ankiet... Ogólnie... MAGYJA.</p>
                </div>
            </section>
        <?php
    }
     
    public function update()
    {
        $this->install();
        
        $oldVer = $settings = get_option( 'seti_watchdog_files__db_version' );
        
        // Update the database version setting.
        update_option( 'seti_watchdog_files__db_version', self::VERSION );

        // Get settings (from db and the default one)
        $settings = get_option( 'seti_watchdog_files__settings' );
        $default_settings = self::getDefaultSettings();

        // For each default setting
        foreach ( $default_settings as $key => $val )
        {
            // Merge the setting
            if ( !isset( $settings[$key] ) )
                $settings[$key] = $val;
        }

        // Update
        update_option( 'seti_watchdog_files__settings', $settings );
    }
}

//Function that just created the object and ignores more...
function initialize_setiWatchdogFiles()
{
    new setiWatchdogFiles();
}

add_action('plugins_loaded', 'initialize_setiWatchdogFiles');
add_action('activate_' . plugin_basename( __FILE__ ), array('setiWatchdogFiles','install'), 1);
