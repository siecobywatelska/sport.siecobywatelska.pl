<?php
/*
Plugin Name: Seti's Admin Locker
Plugin URI: http://wordpress.chibi.pl/plugins/adminlocker
Description: Blocks access to wp-admin for certain users or user levels.
Author: Artur (Seti) Łabudziński
Author URI: http://wordpress.chibi.pl/
Version: 1.0
*/

class setiAdminLocker
{
    const VERSION = '1.0';
    /*
        Po instalacji
            utwórz rolę jak nie ma. (A jak jest to popraw)
        Zablokować możliwość wchodzenia na backend. Jezeli taka rola jest ustawiona.
    */
    
    public function __construct()
    {
        add_action('admin_menu', array($this, 'settings_page' ));
        add_action('admin_init', array($this, 'test'));
    }
    
    
    static protected $userID = null;
    
    public function getCurrentUser()
    {
        global $current_user;
        wp_get_current_user();
        return $current_user;        
    }
    
    //Initiation/installation of the pluging.
    static public function install()
    {
        // Update the database version setting.
        update_option( 'seti_adminlocker__db_version', self::VERSION );
        
        // Get role of only front-end visibility! - integrated thing...
        $role = get_role('front_end_only');
        
        //Create or fix the role...
        if (!empty($role))
        {
            $role->add_cap( 'blocked_bakend' );
            $role->add_cap( 'read' );
            $role->add_cap( 'level_0' );
            $role->add_cap( 'subscriber' );
        }
        else
        {
            add_role
            (
                'front_end_only',
                _x( 'Block Admin Panel', 'role', 'seti_adminlocker' ),
                array
                (
                    'blocked_bakend' => true,
                    'read' => true,
                    'level_0' => true,
                    'subscriber' => true,
                )
            );            
        }
    }
    
    
    static public function debug($var)
    {
        echo '<pre>'; var_dump($var); echo '</pre>';
    } 
    
    public function test()
    {
        $user = $this->getCurrentUser(); 
//        setiAdminLocker::debug($user);
        if (is_admin())
        {
            if ( (in_array('blocked_bakend', $user->allcaps) && !in_array('administrator', $user->allcaps)) || in_array('front_end_only', $user->roles))
            {
                header('Location: /');
                die();
            }
        }
    }
    
    public function settings_page()
    {
        // Add ddefault settings if non found
        if ( false === get_option( 'seti_adminlocker__settings' ) )
                add_option( 'seti_adminlocker__settings', self::getDefaultSettings(), '', 'yes' );

        // Register settings
        add_action( 'admin_init', array($this,'register_settings'));        
    }
    
    protected function getDefaultSettings()
    {
        return array (
            //Ver 1.0
            'url_when_blocked' => '/',
        );
    }
    
    public function update()
    {
        // Update the database version setting.
        update_option( 'seti_adminlocker__db_version', self::VERSION );

        // Get settings (from db and the default one)
        $settings = get_option( 'seti_adminlocker__settings' );
        $default_settings = self::getDefaultSettings();

        // For each default setting
        foreach ( $default_settings as $key => $val )
        {
            // Merge the setting
            if ( !isset( $settings[$key] ) )
                $settings[$key] = $val;
        }

        // Update
        update_option( 'seti_adminlocker__settings', $settings );
    }
    
    public function register_settings()
    {
        register_setting( 'seti_adminlocker__settings', 'seti_adminlocker__settings', 'seti_adminlocker__settings_validate' );        
    }   
}

//Function that just created the object and ignores more...
function initialize_setiAdminLocker()
{
    new setiAdminLocker();
}

add_action('plugins_loaded', 'initialize_setiAdminLocker');
add_action('activate_' . plugin_basename( __FILE__ ), array('setiAdminLocker','install'), 1);
//add_action('admin_init', array('setiAdminLocker','install'), 1);

  