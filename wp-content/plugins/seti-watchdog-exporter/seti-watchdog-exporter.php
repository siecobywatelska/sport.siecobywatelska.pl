<?php
/*
Plugin Name: Seti's Watchdog Ankiety Exporter!
Plugin URI: http://wordpress.chibi.pl/plugins/watchdog/exsporter
Description: Allows Watchdog to export ankiety
Author: Artur (Seti) Łabudziński
Author URI: http://wordpress.chibi.pl/
Version: 1.0
*/

class setiWatchdogExporter
{
    const VERSION = '1.0';
    static protected $url;
    
    public function __construct()
    {
        self::$url = plugin_dir_url(__FILE__);
        add_action('admin_menu', array($this,'doAdminMenu'));
        add_action('wp_ajax_watchdog_export', array($this, 'ajax_watchdog_export'));
    }
  
    //Initiation/installation of the pluging.
    static public function install()
    {
        // Update the database version setting.
        update_option( 'seti_watchdog_exporter__db_version', self::VERSION );
        
    }

    /**
    * Makes admin menu of Watchdog
    *   and users in it.
    * Also creates (later) Seti menu for Seti PlugIns Maintaining.
    */
    public function doAdminMenu()
    {
        add_menu_page('Seti\'s Watchdog Pluggins', 'Watchdog', 'administrator', 'Watchdog', array($this, 'parseAdminPageUsers'), self::$url . 'images/seti.gif', 99);
        add_submenu_page('Watchdog', 'Watchdog Exporter','Eksportuj', 'administrator', 'watchdog_export', array($this, 'parseExportPage'));
    }

    public function parseAdminPageUsers($data)
    {
        ?>
            <section>
                <h2>Watchdog plugin - Exporter</h2>
                <div>
                    <p>Podchodzi pod czarną Magyję. Exportuje ankiety do plików html i tak dalej.</p>
                </div>
            </section>
        <?php
    }
    
    public function ajax_watchdog_export($data)
    {
        global $wpdb;
        
        $return = array (
            'success' => false,
            'message' => '',
        );
        
        if (!isset($_REQUEST['gmina']))
        {
            $return['message'] = 'Brak podanej gminy';
            die(json_encode($return));
        }
        $return['gmina'] = $_REQUEST['gmina'];
        
        $gmina = $wpdb->get_results( $wpdb->prepare('select * from '.$wpdb->prefix.'gmina where nazwa_gminy = %s and isDeleted = 0', $_REQUEST['gmina']) );

        if (count($gmina) == 1)
            $gmina = $gmina[0];
        else
        {
            $return['message'] = '('.$_REQUEST['gmina'].') Brak opisywanej gminy!';
            die(json_encode($return));
        }
        $gmina_id = $wpdb->get_results( $wpdb->prepare('select * from '.$wpdb->prefix.'users where user_login = %s', $_REQUEST['gmina']) );
        if (count($$gmina_id) == 1)
            $gmina_id = $gmina_id[0];

//if (false)
        if (!wp_verify_nonce($_REQUEST['nonce'],'export-gmin:'.$gmina->nazwa_gminy))
        {
            $return['message'] = 'Odświerz stronę i spróbuj ponownie (Błąd uprawnień)';
            die(json_encode($return));
        }
        
        $msg = self::generateGminaFile($gmina, $gmina_id, $gmina->nazwa_gminy.'.zip', true, true);
        
        $return = array_merge($return, $msg);
        
        die(json_encode($return));
    }
    
    
    static public function generateGminaFile($gmina, $gmina_id, $fileName, $recreateFile = true, $_getFiles = false)
    {
$ankietyTitles = array(
    1 => 'Cele gminy w zakresie sportu',
    2 => 'Cele gminy w zakresie sportu',
    3 => 'Zajęcia sportowe dla mieszkańców',
    4 => 'Zajęcia sportowe dla mieszkańców',
    5 => 'Dostęp mieszkańców do obiektów sportowych',
    6 => 'Dostęp mieszkańców do obiektów sportowych',
    7 => 'Informowanie mieszkańców o ofercie sportowej',
    8 => 'Informowanie mieszkańców o ofercie sportowej',
    9 => 'Inne aspekty sportowej polityki gminy',
    10=> 'Inne aspekty sportowej polityki gminy',
    11=> 'Podsumowanie części pierwszej.',
    
    21 => 'Badanie potrzeb i konsultacje społeczne',
    22 => 'Badanie potrzeb i konsultacje społeczne',
    23 => 'Wspieranie inicjatyw mieszkańców',
    24 => 'Wspieranie inicjatyw mieszkańców',
    31 => 'Podsumowanie części drugiej.',
);        
        
        $return = array();
          // Rozpoczynamy export.....
        $files = array();
        { // Złap główne ankiery, revizje i pliki
            //Dodaj plik css'a
            $files[] = array(
                'name' => 'style.css',
                'inline' => true,
                'file' => file_get_contents(plugin_dir_path(__FILE__).'/ankiety.css'),
                'path' => 'ankiety',
            );
            $files[] = array(
                'name' => 'index.html',
                'inline' => true,
                'file' => file_get_contents(plugin_dir_path(__FILE__).'/info.html'),
                'path' => 'ankiety',
            );
        
            $args = array(
                'post_type' => 'gmina_ankieta',
                'posts_per_page' => -1,
                'orderby' => 'menu_order asc',
                'post_author' => $gmina_id[0]->ID,
            );
//            var_dump($args);
            $posts = new WP_Query( $args );
            $ankieta_template = file_get_contents(plugin_dir_path(__FILE__).'/ankieta_template.html');
            if ( $posts -> have_posts() )
            {
                $posts = $posts->get_posts();
                foreach ($posts as $post)
                {
                    if ($post->post_author != $gmina_id[0]->ID) continue;
                    $title = explode(':', $post->post_title);
                    $replaceArray  = array(
                        '{{html}}' => $post->post_content,
                        '{{style}}'=> '../../../css/style.css',
                        '{{title1}}'=> '',
                        '{{title2}}'=> '',
                    );
                    $replaceArray['{{title2}}'] = 'Ankieta '.( ($title[2]&1) ? ($title[2]+1)/2 . ' A' : ($title[2]/2). ' B' );
                    $replaceArray['{{title1}}'] = '['.(int)((($title[2]+1)/2)).']' .$ankietyTitles[(int)(($title[2]))].' '.( ($title[2]&1) ? ' A' : ' B' );
//                    $replaceArray['{{title1}}'] = $ankietyTitles[(int)((($title[2]+1)/2))].' '.( ($title[2]&1) ? ' A' : ' B' );
                    $files[] = array(
                        'name' => 'Ankieta '.( ($title[2]&1) ? ($title[2]+1)/2 . ' A' : ($title[2]/2). ' B' ).'.'.$post->ID.'.html',
                        'inline' => true,
                        'file' => str_replace(array_keys($replaceArray), array_values($replaceArray), $ankieta_template),
                        'path' => 'ankiety/'.$gmina->nazwa_gminy.'/'.(int)((($title[2]+1)/2)),
                    );
                    
                    // Dodaj od razu rewizje
                    $args = array(
                        'post_type' => 'revision',
                        'posts_per_page' => -1,
                        'post_parent' => $post->ID,
                        'post_status' => 'inherit',
                        'menu_order' => $post->menu_order,
                    );
//                var_dump($files);                    if ($title[2] == 31) break;
                    $revs = new WP_Query( $args );
                    if ( $revs -> have_posts() )
                    {
                        $revs = $revs->get_posts();
                        $replaceArray  = array(
//                            '{{html}}' => $post->post_content,
                            '{{style}}' => '../../../../../css/style.css',
                        );                        
                        foreach ($revs as $rev)
                        {
                            $replaceArray['{{title2}}'] = 'Ankieta '.( ($title[2]&1) ? ($title[2]+1)/2 . ' A' : ($title[2]/2). ' B' ).'-'.str_pad($rev->ID,5,'0', STR_PAD_LEFT);
                            $replaceArray['{{title1}}'] = '['.(int)((($title[2]+1)/2)).']' .$ankietyTitles[(int)((($title[2]+1)/2))].' '.( ($title[2]&1) ? ' A' : ' B' );
                            $replaceArray['{{html}}'] = $rev->post_content; 
//                                    $title2 = explode(':', $post->post_title);
                            $files[] = array(
                                'name' => 'Ankieta '.( ($title[2]&1) ? ($title[2]+1)/2 . ' A' : ($title[2]/2). ' B' ).'-'.str_pad($rev->ID,5,'0', STR_PAD_LEFT).'.html',
                                'inline' => true,
                                'file' => str_replace(array_keys($replaceArray), array_values($replaceArray), $ankieta_template),
                                'path' => 'ankiety/'.$gmina->nazwa_gminy.'/'.(int)((($title[2]+1)/2)).'/history',
                            );                                    
                        }
                    }
                    
                    // Teraz dodaj pliki....
                    $args = array(
                        'post_type' => 'gmina_file',
                        'posts_per_page' => -1,
                        'post_parent' => 0,
                        'post_status' => 'private',
                        'menu_order' => $title[2],
                        'post_title' => $gmina->nazwa_gminy,
                    );
                    if ($_getFiles)
                    { // Czy w ogóle łapać pliki do ankiet. PRzydatne będzie potem.
                        $pliki = new WP_Query( $args );
                        if ( $pliki -> have_posts() )
                        {
                            $pliki = $pliki->get_posts();
                            foreach ($pliki as $plik)
                            {
                                $files[] = array(
                                    'name' => $plik->post_content_filtered,
                                    'inline' => false,
                                    'file' => $gmina->nazwa_gminy.'/'.$plik->post_content,
                                    'path' => 'ankiety/'.$gmina->nazwa_gminy.'/'.(($title[2]+1)/2).'/files',
                                );
                            }
                        }
                    }
                }

                $i = 0;
//                file_put_contents(plugin_dir_path(__FILE__).'tmp/'.$gmina->nazwa_gminy.'.zip', 'a');
                $zip = new ZipArchive();
                if ($recreateFile)
                    $opened = $zip->open(plugin_dir_path(__FILE__).'tmp/'.$fileName, ZipArchive::OVERWRITE | ZipArchive::CREATE);
                else
                    $opened = $zip->open(plugin_dir_path(__FILE__).'tmp/'.$fileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);
                    //Tymczasowo naprawiono dla PHP 5.2 poprzez dodanie po | dodatkowego parametru otwierania.
//                var_dump($opened);
                    
                foreach ($files as $file)
                {
                    if ($i > 75)
                    {
//                        echo ' - Reopening file'."\n";
                        $i = 0;
                        $zip->close();
                        $zip = new ZipArchive();
                        $zip->open(plugin_dir_path(__FILE__).'/tmp/'.$fileName);
                    }
//                    echo 'Adding: '.$file['path'] . '/' . $file['name'].' => '.($file['inline'] ? 'inline' : $file['file'])."\n";
                    
                    if ($file['inline'] == true)
                        $zip->addFromString(str_replace('/','\\',$file['path'] . '/' . $file['name']), $file['file']);
                    else
                    {
                        ++$i;
                        if (file_exists(plugin_dir_path(__FILE__).'/../../../front_uploads/'.$file['file']))
//                            echo 'EXISTS: '.plugin_dir_path(__FILE__).'/../../../front_uploads/'.$file['file']."\n";
                            $zip->addFile(plugin_dir_path(__FILE__).'/../../../front_uploads/'.$file['file'], str_replace('/','\\',$file['path'] . '/' . $file['name']));
                    }
                }
                $state = $zip->close();
                if ($state)
                {
                    $return['success'] = true;
                    $return['message'] = '('.$gmina->nazwa_gminy.') File created!';
                }
                
                $return['file'] = plugin_dir_url(__FILE__).'/tmp/'.$fileName;
            }
        }
        
        return $return;
    }

    
    public function parseExportPage($data)
    {
        global $wpdb;
        ?>
            <h1>Administracyjne Eksportowanie Gmin</h1>
            <div>
                <ul id="lista_gmin" style="margin-left: 15px;">
                    <?php
                        $gminy = $wpdb->get_results( $wpdb->prepare('select * from '.$wpdb->prefix.'gmina where isDeleted = 0 order by nazwa_gminy asc', null));
                        foreach ($gminy as $gmina)
                        {
                            ?>
                                <li attr="<?php echo $gmina->nazwa_gminy; ?>" style="clear: both; padding: 0;">
                                    <div class="nazwa" style="width: 140px;"><strong><?php echo str_replace('_', ' ',$gmina->nazwa_gminy); ?></strong></div><div class="password" attr="<?php echo $gmina->nazwa_gminy; ?>" style="width:100px;"></div>
                                    <div class="exporter" attr="<?php echo $gmina->nazwa_gminy; ?>" nonce="<?php echo wp_create_nonce('export-gmin:'.$gmina->nazwa_gminy); ?>">Eksportuj dane gminy</div>
                                </li>
                            <?php
                        }
                    ?>
                </ul>
                <div style="clear:both;" id="messages">
                    <ul>
                    </ul>
                </div>
            </div>
            <script>
                function watchdog_parseAJAXResultExport(ret) {
                    var id = Math.ceil(Math.random()*1000001);
                    var error = jQuery('<li id="error_' + id + '"></li>');
                    if (ret.success == false) {
                        jQuery(error).css('color','red');
                    } else {
                        jQuery(error).css('color','green');
                    }
                    if (typeof(ret.success_color) != 'undefined') jQuery(error).css('color', ret.success_color);
                    
                    if (typeof(ret.message) != "undefined") jQuery(error).html(ret.message);
                    
                    jQuery(error).css('display', "block").appendTo("#messages ul");
                    setTimeout(function () {jQuery('li#error_' + id).fadeOut(2000);}, 500);
                    
                    if (typeof(ret.file) != "undefined") {
                        jQuery('.password[attr="' + ret.gmina + '"]').html('<a href="'+ret.file+'">Plik</a>');
                    }
                }
                
                    // Exportuj dane gminy
                    jQuery(document).on('click', 'li div.exporter', function (o) {
                        if (jQuery(this).hasClass('ajax')) return;
                        var nonce = jQuery(this).attr('nonce');
                        var gmina = jQuery(this).attr('attr');
                        var that = this;
                        jQuery.ajax({
                            url: "<?php echo admin_url('admin-ajax.php'); ?>?action=watchdog_export",
                            type: "POST",
                            data: {command: 'pass', 'nonce': nonce, 'gmina': gmina},
                            dataType: 'json',
                            beforeSend: function () { jQuery('.password[attr="' + gmina + '"]').html('Oczekiwanie...'); },
                            success: function (ret) {
                                watchdog_parseAJAXResultExport(ret);
                            },
                            complete: function () {
                                jQuery(that).removeClass('ajax');
                            }
                        });
                    });
            </script>
            <style>
                #lista_gmin div { float: left; padding: 3px; line-height: 30px; }
                #lista_gmin .exporter { background: url("<?php echo get_template_directory_uri(); ?>/img/icons/save_as.png") no-repeat left center; padding-left: 32px; }
            </style>
        <?php
    }
        
    protected function getDefaultSettings()
    {
        return array (
            //Ver 1.0
        );
    }
    
    public function update()
    {
        // Update the database version setting.
        update_option( 'seti_watchdog_exporter__db_version', self::VERSION );

        // Get settings (from db and the default one)
        $settings = get_option( 'seti_watchdog_exporter__db_version' );
        $default_settings = self::getDefaultSettings();

        // For each default setting
        foreach ( $default_settings as $key => $val )
        {
            // Merge the setting
            if ( !isset( $settings[$key] ) )
                $settings[$key] = $val;
        }

        // Update
        update_option( 'seti_watchdog_exporter__db_version', $settings );
    }
    
}
//Function that just created the object and ignores more...
function initialize_setiWatchdogExporter()
{
    new setiWatchdogExporter();
}

add_action('plugins_loaded', 'initialize_setiWatchdogExporter');
add_action('activate_' . plugin_basename( __FILE__ ), array('setiWatchdogExporter','install'), 1);
//add_action('admin_init', array('setiAdminLocker','install'), 1);

  