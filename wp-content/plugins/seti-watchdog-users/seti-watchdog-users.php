<?php
/*
Plugin Name: Seti's Users and Ankiety plugin for Watchdog
Plugin URI: http://wordpress.chibi.pl/plugins/watchdog/name/users
Description: PlugIn for Watchdog site. Maintain theirs non administrative users in simple manner. Works with Admin Locker PlugIn
Author: Artur (Seti) Łabudziński
Author URI: http://wordpress.chibi.pl/
Version: 1.0
*/

class setiWatchdogUsers
{
    const VERSION = '1.0';
    static protected $url;
    
    public function __construct()
    {
        self::$url = plugin_dir_url(__FILE__);
        add_action('admin_menu', array($this,'doAdminMenu'));
        add_action('admin_bar_menu', array($this, 'doFrontJSbar'),25);
        add_action('admin_bar_menu', array($this, 'doFrontJSbarRemoveOthers'),999);
        add_action('wp_enqueue_scripts', array($this, 'doFrontEndJSbarCSS'),999);
        add_action('wp_ajax_watchdog_ankiety', array($this, 'ajax_watchdog_ankiety'));
        add_action('wp_ajax_watchdog_users', array($this, 'ajax_watchdog_users'));
        add_action('admin_enqueue_scripts', array($this, 'add_admin_scripts'));
        add_action('wp_logout', array($this, 'logOutAction'),999);
    }
    
    public function logOutAction()
    {
        Header('Location: /');
        die();
    }
    
    static public function add_admin_scripts()
    {
        wp_enqueue_style ( 'wp-jquery-ui-dialog');
        wp_enqueue_script( 'jquery-ui-dialog' );
    }
    
    static public function getFileType($type, $file)
    {
//        echo 'Type:' . $type . ' / ' . $file . "\n\n";
        $content = '';
        if (preg_match( '#^audio#', $type ) )
        {
            $meta = wp_read_audio_metadata( $file );

            if ( ! empty( $meta['title'] ) )
                $title = $meta['title'];

            $content = '';

            if ( ! empty( $title ) )
            {
                if ( ! empty( $meta['album'] ) && ! empty( $meta['artist'] ) ) {
                    /* translators: 1: audio track title, 2: album title, 3: artist name */
                    $content .= sprintf( __( '"%1$s" from %2$s by %3$s.' ), $title, $meta['album'], $meta['artist'] );
                } else if ( ! empty( $meta['album'] ) ) {
                    /* translators: 1: audio track title, 2: album title */
                    $content .= sprintf( __( '"%1$s" from %2$s.' ), $title, $meta['album'] );
                } else if ( ! empty( $meta['artist'] ) ) {
                    /* translators: 1: audio track title, 2: artist name */
                    $content .= sprintf( __( '"%1$s" by %2$s.' ), $title, $meta['artist'] );
                } else {
                    $content .= sprintf( __( '"%s".' ), $title );
                }
            } 
            else if ( ! empty( $meta['album'] ) )
            {
                if ( ! empty( $meta['artist'] ) ) {
                    /* translators: 1: audio album title, 2: artist name */
                    $content .= sprintf( __( '%1$s by %2$s.' ), $meta['album'], $meta['artist'] );
                } else {
                    $content .= $meta['album'] . '.';
                }
            } 
            else if ( ! empty( $meta['artist'] ) )
            {
                $content .= $meta['artist'] . '.';
            }

            if ( ! empty( $meta['year'] ) )
                $content .= ' ' . sprintf( __( 'Released: %d.' ), $meta['year'] );

            if ( ! empty( $meta['track_number'] ) )
            {
                $track_number = explode( '/', $meta['track_number'] );
                if ( isset( $track_number[1] ) )
                    $content .= ' ' . sprintf( __( 'Track %1$s of %2$s.' ), number_format_i18n( $track_number[0] ), number_format_i18n( $track_number[1] ) );
                else
                    $content .= ' ' . sprintf( __( 'Track %1$s.' ), number_format_i18n( $track_number[0] ) );
            }

            if ( ! empty( $meta['genre'] ) )
                $content .= ' ' . sprintf( __( 'Genre: %s.' ), $meta['genre'] );
        }
        elseif ( $image_meta = @wp_read_image_metadata( $file ) )
        {
            $sourceImageType = getimagesize( $file );
            $type = $sourceImageType['mime'];
            if ( trim( $image_meta['title'] ) && ! is_numeric( sanitize_title( $image_meta['title'] ) ) )
                $title = $image_meta['title'];
            if ( trim( $image_meta['caption'] ) )
                $content = $image_meta['caption'];
        }
        else
        {
            $sourceImageType = getimagesize( $file );
            $type = $sourceImageType['mime'];
        }

        return array('type'=>$type, 'post_excerp'=>$content);
    }
    
    static public function register_custom_posts()
    {
        register_post_type( 'gmina_files', array(
    //        'labels' => array(
    //            'name' => _x('Media', 'post type general name'),
    //            'name_admin_bar' => _x( 'Media', 'add new from admin bar' ),
    //            'add_new' => _x( 'Add New', 'add new media' ),
    //             'edit_item' => __( 'Edit Media' ),
    //             'view_item' => __( 'View Attachment Page' ),
    //        ),
            'public' => true,
            'show_ui' => false,
    //        '_builtin' => true, /* internal use only. don't use this when registering your own post type. */
    //        '_edit_link' => 'post.php?post=%d', /* internal use only. don't use this when registering your own post type. */
            'capability_type' => 'post',
//            'capabilities' => array(
//                'create_posts' => 'upload_files',
//            ),
    //        'map_meta_cap' => true,
            'hierarchical' => false,
            'rewrite' => false,
            'query_var' => false,
            'show_in_nav_menus' => false,
            'delete_with_user' => false,
            'supports' => array('title', 'author'),
            'rewrite' => false,
            'query_var' => false,
        ) );
        register_post_type( 'gmina_ankieta', array(
    //        'labels' => array(
    //            'name' => _x('Media', 'post type general name'),
    //            'name_admin_bar' => _x( 'Media', 'add new from admin bar' ),
    //            'add_new' => _x( 'Add New', 'add new media' ),
    //             'edit_item' => __( 'Edit Media' ),
    //             'view_item' => __( 'View Attachment Page' ),
    //        ),
            'public' => false,
            'show_ui' => false,
    //        '_builtin' => true, /* internal use only. don't use this when registering your own post type. */
    //        '_edit_link' => 'post.php?post=%d', /* internal use only. don't use this when registering your own post type. */
            'capability_type' => 'post',
    //        'map_meta_cap' => true,
            'hierarchical' => false,
            'rewrite' => false,
            'query_var' => false,
            'show_in_nav_menus' => false,
            'delete_with_user' => false,
            'supports' => array('title', 'author'),
        ) );
    }
    
    public function doFrontEndJSbarCSS()
    {
        // Add this Style - it will fix one thing for them all... (Color of the element after HOVER.)
        wp_enqueue_style('seti_watchdog_user__admin_bar', self::$url.'/css/fixer.css');
    }
    
    
    public function doFrontJSbarRemoveOthers($wp_admin_bar)
    {
        global $current_user;
        global $wp_admin_bar;
        if (is_super_admin()) return;
        if ( (in_array('watchdog_user', $current_user->allcaps) ) )
        {
            if ( !is_admin_bar_showing() )
                return;
        }
        else
            return;
        $wp_admin_bar->remove_menu('site-name');
        $wp_admin_bar->remove_menu('wp-logo');
        $wp_admin_bar->remove_menu('my-account');
        $wp_admin_bar->remove_menu('top-secondary');
    }
    
    /**    
    * Makes entries into JS menu on front-end site. For easier working with the Watchdog Users Permissions
    * 
    * @param wp_admin_bar $wp_admin_bar
    */
    public function doFrontJSbar($wp_admin_bar)
    {
        global $current_user;
        wp_get_current_user();
        
        global $wp_admin_bar;
        
        if ( is_super_admin() || (in_array('watchdog_user', $current_user->allcaps) ) )
        {
            if ( !is_admin_bar_showing() )
                return;
        }
        else
            return;
            
        $wp_admin_bar->add_menu( array(
            'id'     => 'logoutme',
            'title'  => __( 'Wyloguj' ),
            'href'   => wp_logout_url(),
            'meta' => array(
                'class' => 'ab-top-secondary',
            ),
        ) );
        
        
        
        if (!is_super_admin())
            return; /* As of 2013.10.02 */
        
        $wp_admin_bar->add_menu(array(
            'id' => 'watchdog',
            'title' => __('<div class="admin_bar_hover_fixer" style="padding: 0px 5px 0px 25px; background: url(\''.(self::$url . 'images/seti.gif').'\') no-repeat -7px bottom;">Watchdog</div>'),
            'href' => __(''),
        ));
        
        $wp_admin_bar->add_menu(array(
            'parent' => 'watchdog',
            'id'   => 'questionarie',
            'title' => ('Ankiety na 2013 Rok'),
            'href' => __(esc_url( get_permalink( get_page_by_title( 'Questionarie 2013' ) ) )),
        ));
        
        $wp_admin_bar->add_menu(array(
            'parent' => 'watchdog',
            'id'   => 'files',
            'title' => ('Pliki na 2013 Rok'),
            'href' => __(esc_url( get_permalink( get_page_by_title( 'Files 2013' ) ) )),
        ));
        
        $wp_admin_bar->add_menu( array(
            'parent' => 'watchdog',
            'id'     => 'notatnik',
            'title'  => __( 'Notatnik 2012' ),
            'href'   => __(esc_url( get_permalink( get_page_by_title( 'Questionarie 2013 101 Notatnik' ) ) )),
        ) );
    }
    
    /**
    * Makes admin menu of Watchdog
    *   and users in it.
    * Also creates (later) Seti menu for Seti PlugIns Maintaining.
    */
    public function doAdminMenu()
    {
        add_menu_page('Seti\'s Watchdog Pluggins', 'Watchdog', 'administrator', 'Watchdog', array($this, 'parseAdminPageUsers'), self::$url . 'images/seti.gif', 99);
        add_menu_page('Seti\'s Watchdog Pluggins', 'Watchdog', 'administrator', 'Watchdog', array($this, 'parseAdminPageAnkiety'), self::$url . 'images/seti.gif', 99);
        add_submenu_page('Watchdog', 'Watchdog Users','Users', 'administrator', 'watchdog_users', array($this, 'parseUsersPage'));
        add_submenu_page('Watchdog', 'Watchdog Ankiety','Ankiety', 'administrator', 'watchdog_ankiety', array($this, 'parseAnkietyPage'));
    }
    public function ajax_watchdog_ankiety($data)
    {
        global $wpdb;
        
        $command = isset($_REQUEST['command']) ? $_REQUEST['command'] : '';
        $return = array(
            'success' => false,
            'message' => '',
            'command' => $command,
        );
        
        if (!isset($_REQUEST['rev']))
        {
            $return['message'] = 'Brak ID';
            die(json_encode($return));
        }
        
        $post = get_post($_REQUEST['rev']);
        if ($post->ID != $_REQUEST['rev'])
        {
            $return['message'] = 'Brak rewizji';
            die(json_encode($return));
        }
            
        if ($post->post_type != 'revision')
        {
            $return['message'] = 'Nie rewizja';
            die(json_encode($return));
        }
        
        if ($command == 'restore')
        { //Aktywuj treść historyczną (użyj revisji)

            if (!wp_verify_nonce($_REQUEST['nonce'], 'bring_back_ankieta_revision_' . $_REQUEST['rev']))
            {
                $return['message'] = 'Odświerz stronę i spróbuj ponownie (Błąd uprawnień)';
                die(json_encode($return));
            }
            // TODO: Aktywacja revizji polega na: przeniesieniu jej treści do treści posta głównego. oraz na schowaniu rewizji. (utworzenie nowego nonce'a);
            $main = get_post($post->post_parent);
            
            if ($main->post_type != 'gmina_ankieta')
            {
                $_REQUEST['message'] = 'Rewizja nie jest ankietą';
                die(json_encode($return));
            }
            
            $main->post_content = $post->post_content;
            global $current_user;
            $main->post_author = $current_user->ID;
            $main->post_content_filtered = $current_user->user_login;
            wp_update_post($main);
            
            $post->post_title .= ':' . $current_user->ID;
            wp_update_post($post);
            
            $return['content'] = array('rev'=>$main->ID, 'content'=>$main->post_content);            
#            $command = 'trash';
#            $_REQUEST['nonce'] = wp_create_nonce('trash-ankieta-revision-' . $_REQUEST['rev']);
            $return['success'] = true;
        }
        elseif ($command == 'trash') 
        { //Schowaj rewizję totalnie.
        
            if (!wp_verify_nonce($_REQUEST['nonce'],'trash_ankieta_revision_' . $_REQUEST['rev']))
            {
                $return['message'] = 'Odświerz stronę i spróbuj ponownie (Błąd uprawnień)';
                die(json_encode($return));
            }
        
            global $current_user;
            $post->menu_order = 0;
            $post->post_author = $current_user->ID;
            wp_update_post($post);
            
            $return['success'] = true;
            $return['hide'] = $_REQUEST['rev'];
        }
        
        die(json_encode($return));
    }
    
    public function ajax_watchdog_users($data)
    {
        global $wpdb;
        
        $command = isset($_REQUEST['command']) ? $_REQUEST['command'] : '';
        $return = array(
            'success' => false,
            'message' => '',
            'command' => $command,
        );
        
        if ($command != 'install' && !isset($_REQUEST['gmina']))
        {
            $return['message'] = '('.$_REQUEST['gmina'].') Brak Gminy';
            die(json_encode($return));
        }
        
        $gmina = null;
        if ($command != 'create' && $command != 'install')
        {
            $gmina = $wpdb->get_results( $wpdb->prepare('select * from '.$wpdb->prefix.'gmina where nazwa_gminy = %s and isDeleted = 0', $_REQUEST['gmina']) );
            if (count($gmina) == 1)
                $gmina = $gmina[0];
            else
            {
                $return['message'] = '('.$_REQUEST['gmina'].') Brak opisywanej gminy!';
                die(json_encode($return));
            }
        }
        
        if ($command == 'pass')
        {
            if (!wp_verify_nonce($_REQUEST['nonce'], 'recover-pass-for:'.$_REQUEST['gmina']))
            {
                $return['message'] = 'Odświerz stronę i spróbuj ponownie (Błąd uprawnień)';
                die(json_encode($return));
            }
            
            $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
            $user = get_user_by('login', $_REQUEST['gmina']);
            if (!$user)
            {
                $return['message'] = '('.$_REQUEST['gmina'].') Taka gmina nie istnieje w bazie...';
                die(json_encode($return));
            }
            
            $user->user_pass = $random_password;
            $s = wp_update_user($user);
            if ($s)
            {
                $return['success'] = true;
                $return['message'] = 'Hasło zmienione ('.$_REQUEST['gmina'].')';
                $return['gmina'] = array('gmina'=>$_REQUEST['gmina'], 'pass'=>$random_password, 'nonce' => wp_create_nonce('recover-pass-for:'.$_REQUEST['gmina']));
            }
        }
        elseif ($command == 'install')
        {
            if (!wp_verify_nonce($_REQUEST['nonce'], 'reinstall:gmina_user:plugin'))
            {
                $return['message'] = 'Odświerz stronę i spróbuj ponownie (Błąd uprawnień)';
                die(json_encode($return));
            }
            
            self::install();
            $return['success'] = true;
            $return['message'] = 'Reinicjalizacja ról - wykonana';
            $return['nonce'] = wp_create_nonce('reinstall:gmina_user:plugin');
        }
        elseif ($command == 'delete')
        {
            if (!wp_verify_nonce($_REQUEST['nonce'], 'remove-gmina:'.$_REQUEST['gmina']))
            {
                $return['message'] = 'Odświerz stronę i spróbuj ponownie (Błąd uprawnień)';
                die(json_encode($return));
            }
            
            
            $sql = 'UPDATE '.$wpdb->prefix.'gmina SET isDeleted = 1  WHERE nazwa_gminy = %s';
            $s = $wpdb->query( $wpdb->prepare( $sql, $_REQUEST['gmina']) );

            if ($s > 0)
            {
                $return['success'] = true;
                $return['hide'] = $_REQUEST['gmina'];
                $return['message'] = '('.$_REQUEST['gmina'].') Usunięto gminę';
            }
            else
            {
                $return['message'] = '('.$_REQUEST['gmina'].') Nie udało się usunąć gminy';
            }
        }
        elseif ($command == 'create')
        { // Utwórz nową gminę i jej konto i hasełko dla niej.
            if (!validate_username($_REQUEST['gmina']))
            {
                $return['message'] = '('.$_REQUEST['gmina'].') Nazwa gmina zawiera znaki przeszkadzające utworzeniu konta... Prosze to poprawić';
                die(json_encode($return));
            }
            
            if (!wp_verify_nonce($_REQUEST['nonce'], 'add-new-gmina:'))
            {
                $return['message'] = 'Odświerz stronę i spróbuj ponownie (Błąd uprawnień)';
                die(json_encode($return));
            }
            
            //Utwórz usera dla nazwy gminy.
            $_REQUEST['gmina'] = str_replace(' ','_', $_REQUEST['gmina']);
            $user_id = username_exists( $_REQUEST['gmina'] );
            if ( !$user_id and email_exists('gmina.'.$_REQUEST['gmina'] . '@watchdog.pl') == false )
            {
                $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
//                $user_id = wp_create_user( $user_name, $random_password, $user_email );
                $user = array(
                    'user_pass' => $random_password,
                    'user_login' => $_REQUEST['gmina'],
                    'user_nicename' => 'Ankieterzy gminy: '.$_REQUEST['gmina'],
                    'user_email' => 'gmina.'.$_REQUEST['gmina'] . '@watchdog.pl',
                    'role' => 'front_end_only',
                );
                $user['ID'] = wp_insert_user ($user);
                if ($user['ID'] * 1 > 0)
                {
                    $s = $wpdb->insert($wpdb->prefix.'gmina', array('nazwa_gminy'=>$_REQUEST['gmina']));
                    if ($s > 0)
                    {
                        $return['message'] = 'Konto utworzone';
                        $return['success'] = true;
                        $return['nonce'] = wp_create_nonce('add-new-gmina:');
                        $return['gmina'] = array('gmina'=>$_REQUEST['gmina'], 'pass'=>$random_password, 'nonce' =>  wp_create_nonce('recover-pass-for:'.$_REQUEST['gmina']), 'del_nonce' => wp_create_nonce('remove-gmina:'.$_REQUEST['gmina']));
                        
                        $user = get_user_by('id', $user['ID']);
                        $user->set_role('front_end_only');
                    }
                    else
                    {
                        $return['message'] = 'Nie udało się utworzyć wpisu gminy...';
                        die(json_encode($return));
                    }
                }
                else
                {
                    $s = $wpdb->delete($wpdb->prefix.'users', array('user_login'=>$_REQUEST['gmina']),array('%s'));
                    $return['message'] = 'Nie udało się utworzyć konta gminy';
                    die(json_encode($return));
                }
            }
            else
            {
                $gmina = $wpdb->get_results( $wpdb->prepare('Select * from '.$wpdb->prefix.'gmina where nazwa_gminy = %s', $_REQUEST['gmina']));
                if (count($gmina) > 0) $gmina = $gmina[0];
                if ($gmina->isDeleted == 1)
                {
                    $sql = 'UPDATE '.$wpdb->prefix.'gmina SET isDeleted = 0  WHERE nazwa_gminy = %s';
                    $s = $wpdb->query( $wpdb->prepare( $sql, $_REQUEST['gmina']) );
                    if ($s > 0)
                    {
                        $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
                        $user = get_user_by('id', $user_id);
                        $user->user_pass = $random_password;
                        $user->set_role('front_end_only');
                        wp_update_user($user);
                        $return['success'] = true;
                        $return['success_color'] = 'blue';
                        $return['nonce'] = wp_create_nonce('add-new-gmina:');
                        $return['gmina'] = array('gmina'=>$_REQUEST['gmina'], 'pass'=>$random_password, 'nonce' =>  wp_create_nonce('recover-pass-for:'.$_REQUEST['gmina']), 'del_nonce' => wp_create_nonce('remove-gmina:'.$_REQUEST['gmina']));
                        $return['message'] = '('.str_replace('_',' ',$_REQUEST['gmina']).') Konto dla gminy zostało odświerzone (istniało wcześniej, ale było usunięte)';
                        die(json_encode($return));
                    }
                    else
                    {
                        $return['message'] = '('.str_replace('_',' ',$_REQUEST['gmina']).') Konto gminy istnieje, ale nie udało się go przywrócić';
                        $return['nonce'] = wp_create_nonce('add-new-gmina:');
                        die(json_encode($return));
                    }
                }

                $return['message'] = '('.str_replace('_',' ',$_REQUEST['gmina']).') Takie konto dla gminy już istnieje. Proszę wybrać inne';
                $return['nonce'] = wp_create_nonce('add-new-gmina:');
                die(json_encode($return));
            }            
        }
        
        die(json_encode($return));        
    }
    
    public function parseAnkietyPage($data)
    {
        ?>
            <div id="watchdog_ankieta_admin">
                <script>
                    jQuery(document).ready( function () {
                        jQuery('ul.gminy li').click( function () {
                            jQuery("li.ankieta-element").fadeOut(100);
                            jQuery('li.ankieta-element[attr="' + jQuery(this).attr('attr') + '"]').fadeIn(300);
                        });
                    });
                    
                    function watchdog_parseAJAXResult(ret) {
//                        console.log(ret);
                        if (ret.success == false) {
                            jQuery("#watchdog_error div").stop().fadeOut(0).stop().html(ret.message).fadeIn(100).fadeOut(1000);
                        }
                        
                        if (typeof(ret.content) != "undefined") {
                            jQuery("#ankieta-id-" + ret.content.rev + ' .content_inner').html(ret.content.content);
                            console.log("#ankieta-id-" + ret.content.rev + ' .content_inner => ' + ret.content.content);
                        }
                        
                        if (typeof(ret.hide) != "undefined") {
//                            console.log("Hiding: " + ret.hide);
                            jQuery("#ankieta-id-" + ret.hide).remove();
                            jQuery('li[rev="' + ret.hide + '"]').remove();
                        }
                    }
                
                    jQuery(document).ready( function () {
                        jQuery("#watchdog_ankieta_admin .opcje .history").click( function () {1
//                        console.log(this);
                            setTimeout(function () {jQuery("#wpbody").css('height', Math.max(500, jQuery("ul#bazowa").height()+100) + 'px');}, 1000);

//                            if (jQuery(this).hasClass('notshown')) {
//                                jQuery(this).html('Schowaj Historię Edycji').removeClass('notshown');
                                jQuery("#revisions").html(jQuery('#watchdog_ankieta_admin #ankieta-id-' + jQuery(this).attr('rev') + ' .revisions').html());
                                //jQuery('#watchdog_ankieta_admin #ankieta-id-' + jQuery(this).attr('rev') + ' .revisions').fadeIn(1000);
                                
//                            } else {
//                                jQuery(this).html('Zobacz historię edycji').addClass('notshown');
//                                jQuery("#revisions").html("");
                                //jQuery('#watchdog_ankieta_admin #ankieta-id-' + jQuery(this).attr('rev') + ' .revisions').fadeOut(1000);
//                            }
                        });
                        
                        jQuery(document).ready(function () {
                            setTimeout(function () {jQuery("#wpbody").css('height', Math.max(500, jQuery("ul#bazowa").height()+100) + 'px');}, 1000);
                            var $info = jQuery("#content_holder");
                            $info.dialog({
                                'width'         : 'auto',
                                'left'          : 'auto',                   
                                'dialogClass'   : 'wp-dialog',           
                                'modal'         : true,
                                'autoOpen'      : false, 
                                'closeOnEscape' : true,
                                'closeText'     : 'Zamknij',      
//                                'buttons'       : {
//                                    "Close": function() {
//                                        jQuery(this).dialog('close');
//                                    }
//                                }
                            });                            
                        })
                        
                        function setiWatchdogAnkiety_dialog(title, content) {
                            var $info = jQuery("#content_holder");
                            $info.html('<div style="padding:10px;"><h2>' + title + "</h2>" + content + '</div>').dialog('open', {title: title});
                        }
                        
                        jQuery("#watchdog_ankieta_admin .opcje .content").click( function () {
                            var rev = jQuery(this).attr('rev');
                            setiWatchdogAnkiety_dialog(jQuery('#ankieta-id-' + rev + ' .name').html(), jQuery('#ankieta-id-' + rev + ' .content_inner').html());
                            //jQuery("#content_holder .content").html(jQuery('#ankieta-id-' + rev + ' .content_inner').html()).show();
                            //jQuery("#content_holder h2").html(jQuery('#ankieta-id-' + rev + ' .name').html());
                        });
                        jQuery(document).on('click', "#watchdog_ankieta_admin #revisions .see", function () {
                            var rev = jQuery(this).parent().attr('rev');
                            setiWatchdogAnkiety_dialog('Rewizja: ' + rev, jQuery('li[rev="' + rev + '"] .content').html());                            //jQuery("#content_holder .content").html(jQuery('li[rev="' + rev + '"] .content').html()).show();
                            //jQuery("#content_holder h2").html('Rewizja: ' + rev);
                        });
                        jQuery(document).on('click', "#watchdog_ankieta_admin #revisions .przywroc", function () {
                            if (jQuery(this).hasClass('ajax')) return;
                            var rev = jQuery(this).attr('rev');
                            var nonce = jQuery(this).attr('nonce');
                            var that = this;
                            jQuery.ajax({
                                url: "<?php echo admin_url('admin-ajax.php'); ?>?action=watchdog_ankiety",
                                type: "POST",
                                data: {'rev': rev, command: 'restore', 'nonce': nonce},
                                dataType: 'json',
                                success: function (ret) {
                                    watchdog_parseAJAXResult(ret);
                                },
                                complete: function () {
                                    jQuery(that).removeClass('ajax');
                                }
                            });
                        });
                        jQuery(document).on('click', "#watchdog_ankieta_admin #revisions .usun", function () {
                            if (jQuery(this).hasClass('ajax')) return;
                            var rev = jQuery(this).attr('rev');
                            var nonce = jQuery(this).attr('nonce');
                            var that = this;
                            jQuery.ajax({
                                url: "<?php echo admin_url('admin-ajax.php'); ?>?action=watchdog_ankiety",
                                type: "POST",
                                data: {'rev': rev, command: 'trash', 'nonce': nonce},
                                dataType: 'json',
                                success: function (ret) {
                                    watchdog_parseAJAXResult(ret);
                                },
                                complete: function () {
                                    jQuery(that).removeClass('ajax');
                                }
                            });                            
                        });
                    });
                </script>
                <div id="watchdog_error">&nbsp;<div></div></div>
                <?php
                    $args = array(
                        'post_type' => 'gmina_ankieta',
                        'posts_per_page' => -1,
                        'orderby' => 'post_title asc',
                    );
                    $posts = new WP_Query( $args );
                    if ( $posts -> have_posts() )
                    {
                        $posts = $posts->get_posts();
                        $osoby = array();
                        foreach ($posts as $post)
                        {
                            $title = explode(':', $post->post_title);
                            $osoby[$title[1]][$title[2]] = $post;
                        }
                        
                        echo '<ul class="gminy">';
                        foreach ($osoby as $key => $osoba)
                        {
                            ?>
                                <li attr="<?php echo $key; ?>">Gmina: <em><?php echo str_replace('_', ' ',$key); ?></em></li>
                            <?php
                        }
                        echo '</ul>';
                        
                        echo '<ul id="bazowa">';
                        ?>
                                <li class="header">
                                    <section>
                                        <header>
<!--                                            <div class="gmina">Nazwa Gminy</div>-->
                                            <div class="title">Artykuł</div>
                                            <div class="opcje">Opcje</div>
                                        </header>
                                    </section>
                                </li>
                        <?php

                        
                        $i = 0;
                        foreach ($osoby as $gmina => $posty)
                        foreach ($posty as $ankieta => $post)
                        {
        //var_Dump($post);
                            $title = explode(':', $post->post_title);
                            ?>
                                <li id="ankieta-id-<?php echo $post->ID; ?>" class="hidden ankieta-element" attr="<?php echo $title[1]; ?>">
                                    <section>
                                        <header>
<!--                                            <div class="gmina"><?php echo $title[1]; ;?></div> -->
                                            <div class="name">Ankieta część <?php echo ($title[2]&1) ? ($title[2]+1)/2 . ' A' : ($title[2]/2) . ' B'; ?></div>
                                            <div class="content_inner hidden"><?php echo $post->post_content; ?></div>
                                            <div class="opcje">
                                                <div class="history notshown" rev="<?php echo $post->ID; ?>">Zobacz historię edycji</div>
                                                <div class="content notshown" rev="<?php echo $post->ID; ?>">Zobacz treść ankiety</div>
                                            </div>
                                        </header>
                                        <div class="revisions hidden">
                                            <?php
                                                $post->post_title;
                                            ?>
                                            <h3>Historia rewizji dla ankiety <?php echo $post->post_title; ?> - <?php echo $gmina; ?></h3>
                                            <?php
                                                $args = array(
                                                    'post_type' => 'revision',
                                                    'posts_per_page' => -1,
            //                                        'orderby' => 'post_title asc',
                                                    'post_parent' => $post->ID,
                                                    'post_status' => 'inherit',
                                                    'menu_order' => $post->menu_order,
                                                );
                                                $revs = new WP_Query( $args );
                                                if ( $revs -> have_posts() )
                                                {
                                                    echo '<ul class="revs" attr="'.$post->ID.'">';
                                                    $revs = $revs->get_posts();
                                                    foreach ($revs as $rev)
                                                    {
                                                        ?>
                                                            <li rev="<?php echo $rev->ID; ?>">
                                                                <div class="date"><?php echo $rev->post_date; ?></div>
                                                                <div class="content hidden"><?php echo $rev->post_content; ?></div>
                                                                <div class="see">Zobacz treść rewizji</div>
                                                                <div class="przywroc" post="<?php echo $post->ID; ?>" rev="<?php echo $rev->ID; ?>" nonce="<?php echo wp_create_nonce('bring_back_ankieta_revision_'.$rev->ID); ?>">PRZYWRÓĆ REWIZJĘ</div>
                                                                <div class="usun" post="<?php echo $post->ID; ?>" rev="<?php echo $rev->ID; ?>" nonce="<?php echo wp_create_nonce('trash_ankieta_revision_'.$rev->ID); ?>">USUŃ REWIZJĘ</div>
                                                            </il>
                                                        <?php
                                                    }
                                                    echo '</ul>';
                                                }
                                                else
                                                    echo 'Brak rewizji...';
                                                                                        
                                            ?>
                                        </div>
                                    </section>
                                </li>
                            <?php
                        }
                        echo '</ul>';
                        echo '<ul id="revisions"></ul>';
                    }
                ?>
            </div>
            <div id="content_holder" style="float: left; display:none;">
                <h2></h2>
                <div class="content"></div>
            </div>
			<style type="text/css">
			   #wpwrap #wpcontent #wpbody #watchdog_ankieta_admin{
			       float: left;
			   }
			   #wpwrap #wpcontent #wpbody #watchdog_ankieta_admin ul li.header section header{
			       height: 20px;
			   }
			   #wpwrap #wpcontent #wpbody #watchdog_ankieta_admin ul li.header section header>div{
			      background-color: #ECECEC;
			       float: left;
			       font-weight: bold;
			   }
			   #wpwrap #wpcontent #wpbody #watchdog_ankieta_admin ul li.ankieta-element{
			       width: 99%;
			       min-height: 65px;
			       border-bottom: 1px solid #CCC;
			   }
			   #wpwrap #wpcontent #wpbody #watchdog_ankieta_admin ul li.ankieta-element section header>div{
			       float:left;
			   }
			   #wpwrap #wpcontent #wpbody #watchdog_ankieta_admin ul li.ankieta-element section header .gmina{
			       font-weight: bold;
			       font-size: 25px;
			       padding-top: 20px;
			   }
			   #wpwrap #wpcontent #wpbody #watchdog_ankieta_admin ul li.ankieta-element section header .opcje>div{
			       /*margin-bottom: 5px;*/
			       line-height: 32px;
			   }
			   #wpwrap #wpcontent #wpbody #watchdog_ankieta_admin ul li.ankieta-element section header .opcje .history{
			       background: url('../wp-content/plugins/seti-watchdog-users/images/calendar.png') no-repeat left center;
			       padding-left: 32px;    
			       text-align: left;
			   }
			   #wpwrap #wpcontent #wpbody #watchdog_ankieta_admin ul li.ankieta-element section header .opcje .content{
			        background: url('../wp-content/plugins/seti-watchdog-users/images/document_file.png') no-repeat left center;
			       padding-left: 32px;    
			       text-align: left;
			   }
			   #wpwrap #wpcontent #wpbody #content_holder{
			       margin-top: 28px;
			       width: 737px;
			       float: right;
			       border: 1px solid gray;
			       background-color: #ECECEC;
			       padding: 5px;
			   }
               
               ul#gminy
               {
position: fixed;
               }
               
               ul#bazowa .opcje { width: 165px; }
               ul#bazowa .title, ul#bazowa .name { width: 135px; }
               ul#bazowa
               {
/*                   float: left;*/
left: 200px;
width: 310px;
position: absolute;
top: 17px;             
                   
               }
               
               ul#revisions
               {
position: fixed;
left: 660px;
top: 29px;                   
                   float: left;
                   margin-left: 15px;
               }
			</style>
        <?php
    }
    
    /**
    * Page for taking the settings for Watchdog User`s,  and do its stuff
    * 
    */
    public function parseAdminPageUsers($data)
    {
        ?>
            <section>
                <h2>Watchdog plugin - Users</h2>
                <div>
                    <p>Moduł pozwalający na tworzenie gmin (userów o ich uprawnieniach). Pilnujący uprawnień gmin i robiący trochę innej magii...</p>
                </div>
            </section>
        <?php
    }
    
    public function parseAdminPageAnkiety($data)
    {
        ?>
            <section>
                <h2>Watchdog plugin - Ankiety</h2>
                <div>
                    <p>Jeszcze większa MAGYJA - pozwala na zabawy z ankietami przez ankieterów. Dla administratorów jest za to moduł exportujący dane oraz przywracanie i usuwanie kopi poedycyjnych.</p>
                </div>
            </section>
        <?php
    }

    public function parseUsersPage($data)
    {
        global $wpdb;
        ?>
            <h1>Zarządzanie gminami i ich kontami</h1>
            <h2>Lista</h2>
            <div>
                <ul id="lista_gmin" style="margin-left: 15px;">
                    <?php
                        $gminy = $wpdb->get_results( $wpdb->prepare('select * from '.$wpdb->prefix.'gmina where isDeleted = 0 order by nazwa_gminy asc', null));
                        foreach ($gminy as $gmina)
                        {
                            ?>
                                <li attr="<?php echo $gmina->nazwa_gminy; ?>" style="clear: both; padding: 0;">
                                    <div class="nazwa" style="width: 140px;"><strong><?php echo str_replace('_', ' ',$gmina->nazwa_gminy); ?></strong></div><div class="password" attr="<?php echo $gmina->nazwa_gminy; ?>" style="width:100px;"></div>
                                    <?php
                                        if ($gmina->type == null)
                                        {
                                            ?>
                                                <div class="deleter" attr="<?php echo $gmina->nazwa_gminy; ?>" nonce="<?php echo wp_create_nonce('remove-gmina:'.$gmina->nazwa_gminy); ?>">Usuń</div>
                                            <?php
                                        }
                                        else
                                            wp_create_nonce('remove-gmina:'.$gmina->nazwa_gminy);
                                    ?>
                                    <div class="pass_changer" attr="<?php echo $gmina->nazwa_gminy; ?>" nonce="<?php echo wp_create_nonce('recover-pass-for:'.$gmina->nazwa_gminy); ?>">Wygeneruj nowe hasło</div>
                                </li>
                            <?php
                        }
                    ?>
                </ul>
                <div id="dodaj_gmine">
                    <div id="new_gmina_execute" nonce="<?php echo wp_create_nonce('add-new-gmina:'); ?>">Dodaj nową gminę*</div>
                    <div class="label">* - Uwaga - dodaje to także nowe konto do strony z losowym hasłem. Hasło to zostanie wyświetlone RAZ</div>
                </div>
                <div nonce="<?php echo wp_create_nonce('reinstall:gmina_user:plugin'); ?>" style="clear: both; margin-top: 100px;" id="reinstall">Przeładuj plugin (poprawia uprawnienia dla kont o zablokowanym dostępie do backendu (konta dla gmin))</div>
                <div id="messages">
                    <ul>
                    </ul>
                </div>
            </div>
            <script>
                function watchdog_parseAJAXResultUser(ret) {
                    var id = Math.ceil(Math.random()*1000001);
                    var error = jQuery('<li id="error_' + id + '"></li>');
                    if (ret.success == false) {
                        jQuery(error).css('color','red');
                    } else {
                        jQuery(error).css('color','green');
                    }
                    if (typeof(ret.success_color) != 'undefined') jQuery(error).css('color', ret.success_color);
                    
                    jQuery(error).html(ret.message);
                    
                    if (ret.command == 'create') {
                        jQuery('#dodaj_gmine #new_gmina_execute').attr('nonce', ret.nonce);
                    } else if (ret.command == 'install') {
                        jQuery('#reinstall').attr('nonce', ret.nonce);
                    }
                    
                    if (typeof(ret.hide) != 'undefined') {
                        jQuery('li[attr="' + ret.hide + '"]').fadeOut(2000);
                    }
                    
                    if (typeof(ret.gmina) != 'undefined') {
                        var found = jQuery('li[attr="' + ret.gmina.gmina + '"]');
                        if (jQuery('li[attr="' + ret.gmina.gmina + '"]').length > 0) {
                            jQuery('.password', found).html(ret.gmina.pass);
                            jQuery('.pass_changer', found).attr('nonce',ret.gmina._);
                            if (typeof(ret.del_none) != 'undefined') jQuery('.deleter', found).attr('nonce', ret.gmina.del_nonce);
                            if (jQuery(found).css('display') == 'none') jQuery(found).fadeIn(300);
                        } else {
                            console.log('Adding elemenet');
                            var elem = jQuery('<li attr="' + ret.gmina.gmina + '" style="display: none; clear: both; padding: 0;"><div class="nazwa" style="width: 140px;"><strong>' + ret.gmina.gmina.replace('_',' ') + '</strong></div><div class="password" attr="' + ret.gmina.gmina + '" style="width:100px;">' + ret.gmina.pass + '</div><div class="deleter" attr="' + ret.gmina.gmina + '" nonce="' + ret.gmina.del_nonce + '">Usuń</div><div class="pass_changer" attr="' + ret.gmina.gmina + '" nonce="' + ret.gmina.nonce + '">Wygeneruj nowe hasło</div></li>');
                            jQuery(elem).appendTo("ul#lista_gmin");
                            setTimeout( function () {jQuery('li[attr="' + ret.gmina.gmina + '"]').fadeIn(500);}, 500);
                            console.log('added');
                        }
                    }
                    
                    jQuery(error).css('display', "block").appendTo("#messages ul");
                    setTimeout(function () {jQuery('li#error_' + id).fadeOut(2000);}, 500);
                }
            
                jQuery(document).ready( function () {
                    // Dodanie nowej gminy
                    jQuery('#dodaj_gmine #new_gmina_execute').click( function (o) {
                        if (jQuery(this).hasClass('ajax')) return;
                        var nonce = jQuery(this).attr('nonce');
                        var that = this;
                        
                        var gmina = prompt("Podaj nazwę nowej gminy do utworzenia. Proszę pamiętać o nieużywaniu polskich znaków! ", '');
                        if (gmina == '-' || gmina == '')
                            return;
                        
                        jQuery.ajax({
                            url: "<?php echo admin_url('admin-ajax.php'); ?>?action=watchdog_users",
                            type: "POST",
                            data: {command: 'create', 'nonce': nonce, 'gmina': gmina},
                            dataType: 'json',
                            success: function (ret) {
                                if (typeof(ret.nonce) != 'undefined') {
                                    jQuery(that).attr('nonce', ret.nonce);
                                }
                                watchdog_parseAJAXResultUser(ret);
                            },
                            complete: function () {
                                jQuery(that).removeClass('ajax');
                            }
                        });
                    });
                                        
                    // Reset hasła do gminy
                    jQuery(document).on('click', 'li div.pass_changer', function (o) {
                        if (jQuery(this).hasClass('ajax')) return;
                        var nonce = jQuery(this).attr('nonce');
                        var gmina = jQuery(this).attr('attr');
                        var that = this;
                        jQuery.ajax({
                            url: "<?php echo admin_url('admin-ajax.php'); ?>?action=watchdog_users",
                            type: "POST",
                            data: {command: 'pass', 'nonce': nonce, 'gmina': gmina},
                            dataType: 'json',
                            success: function (ret) {
                                watchdog_parseAJAXResultUser(ret);
                            },
                            complete: function () {
                                jQuery(that).removeClass('ajax');
                            }
                        });
                    });
                    
                    // Usunięcie gminy
                    jQuery(document).on('click', 'li div.deleter', function (o) {
                        if (jQuery(this).hasClass('ajax')) return;
                        var nonce = jQuery(this).attr('nonce');
                        var gmina = jQuery(this).attr('attr');
                        var that = this;
                        jQuery.ajax({
                            url: "<?php echo admin_url('admin-ajax.php'); ?>?action=watchdog_users",
                            type: "POST",
                            data: {command: 'delete', 'nonce': nonce, 'gmina': gmina},
                            dataType: 'json',
                            success: function (ret) {
                                watchdog_parseAJAXResultUser(ret);
                            },
                            complete: function () {
                                jQuery(that).removeClass('ajax');
                            }
                        });
                    });
                    
                    // ,,Przeliczenie'' uprawnień userów (install dla $this);
                    jQuery('#reinstall').click( function () {
                        if (jQuery(this).hasClass('ajax')) return;
                        var nonce = jQuery(this).attr('nonce');
                        var that = this;
                        jQuery.ajax({
                            url: "<?php echo admin_url('admin-ajax.php'); ?>?action=watchdog_users",
                            type: "POST",
                            data: {command: 'install', 'nonce': nonce},
                            dataType: 'json',
                            success: function (ret) {
                                watchdog_parseAJAXResultUser(ret);
                            },
                            complete: function () {
                                jQuery(that).removeClass('ajax');
                            }
                        });
                    });
                });
            </script>
            <style>
                #lista_gmin div { float: left; padding: 3px; }
                #dodaj_gmine { clear: both; }
                #dodaj_gmine input, #dodaj_gmine div { float: left; }
                #dodaj_gmine div.label { clear: both; }
                #lista_gmin .deleter { background: url("<?php echo get_template_directory_uri(); ?>/img/icons/stop_2.png") no-repeat left center; padding-left: 32px; }
                #lista_gmin .pass_changer { background: url("<?php echo get_template_directory_uri(); ?>/img/icons/lamp_active.png") no-repeat left center; padding-left: 32px; }
                #new_gmina_execute { background: url("<?php echo get_template_directory_uri(); ?>/img/icons/check.png") no-repeat left center; padding-left: 32px; }
            </style>
        <?php
    }
    
    
    /**
    * Sprawdź czy była już jakaś wcześniej instalacja.
    * 
    */
    static public function install()
    {
        global $wpdb;
        // Update the database version setting.
        update_option( 'seti_watchdog_users__db_version', self::VERSION );

        // Get role of only front-end visibility! - integrated thing...
        $role = get_role('front_end_only');
        
        //Create or fix the role...
        if (!empty($role))
        {
            $role->add_cap( 'watchdog_user' );
            $role -> add_cap('read');
            $role -> add_cap('read_private_pages');
        }
        else
        {
            $role = get_role('watchdog_user');
            
            if (!empty($role))
            {
                $role -> add_cap('watchdog_user');
                $role -> add_cap('read');
                $role -> add_cap('subscriber');
                $role -> add_cap('level_0');
                $role -> add_cap('read_private_posts');
                $role -> add_cap('read_private_pages');
            }
            else
            {
                add_role
                (
                    'watchdog_user',
                    _x( 'Watchdog User', 'role', 'seti_watchdog__users' ),
                    array
                    (
                        'read' => true,
                        'level_0' => true,
                        'subscriber' => true,
                        'watchdog_user' => true,
                        'read_private_posts' => true,
                        'read_private_pages' => true,
                    )
                );
            }            
        }
    }
    
    
    static protected function getDefaultSettings()
    {
        return array(
            '' => ''
        );
    }
    
     
    public function update()
    {
        $this->install();
        
        $oldVer = $settings = get_option( 'seti_watchdog_users__db_version' );
        
        // Update the database version setting.
        update_option( 'seti_watchdog_users__db_version', self::VERSION );

        // Get settings (from db and the default one)
        $settings = get_option( 'seti_watchdog_users__settings' );
        $default_settings = self::getDefaultSettings();

        // For each default setting
        foreach ( $default_settings as $key => $val )
        {
            // Merge the setting
            if ( !isset( $settings[$key] ) )
                $settings[$key] = $val;
        }

        // Update
        update_option( 'seti_watchdog_users__settings', $settings );
    }
    
    static protected $gmina = null;
    static protected $fullGmina = null;
    static public function getGmina($refresh = false)
    {
        if ($refresh == false && self::$gmina != null)
            return self::$gmina;

        global $wpdb;
        global $current_user;
        $login_gmina = $current_user -> user_login;
        $current_gmina = $wpdb->get_results( $wpdb->prepare('Select * from '.$wpdb->prefix.'gmina where nazwa_gminy = %s and isDeleted = 0', $login_gmina));
        if (count($current_gmina) > 0)
        {
            self::$gmina = $current_gmina[0];
        }
        else
            self::$gmina = false;
        
        return self::$gmina;
    }

    static public function getAllGminy($refresh = false)
    {
        $return = array();
        
        global $wpdb;
        global $current_user;
        $current_gmina = $wpdb->get_results( $wpdb->prepare('Select * from '.$wpdb->prefix.'gmina where isDeleted = 0 order by nazwa_gminy', 0));
        foreach($current_gmina as $gmina)
        {
#            print_r($gmina);
            $return[] = self::getGminaDataByID($gmina);
        }
        
        return $return;
    }

    static public function getGminaDataByID($gmina)
    {
//        $gmina = self::getGmina($refresh);
//        if ($refresh == true && self::$fullGmina != null)
//            return self::$fullGmina;
        
        global $wpdb;
        $cats = $wpdb->get_results( $wpdb->prepare('select * from '.$wpdb->prefix.'gmina_obiekty where nazwa_gminy = %s', $gmina->nazwa_gminy));
        foreach ($cats as $cat)
            $gmina->kategoria[] = array('name'=>$cat->Kategoria, 'ilosc'=>$cat->ilosc);

        $klubs = $wpdb->get_results( $wpdb->prepare('select * from '.$wpdb->prefix.'gmina_kluby where nazwa_gminy = %s', $gmina->nazwa_gminy));
        foreach ($klubs as $klub)
            $gmina->klub[] = array('name'=>$klub->Kategoria, 'ilosc'=>$klub->ilosc);
        
        $persons = $wpdb->get_results( $wpdb->prepare('select osoba from '.$wpdb->prefix.'gmina_persons where nazwa_gminy = %s', $gmina->nazwa_gminy));
        foreach ($persons as $person)
            $gmina->person[] = $person->osoba;
        //self::$fullGmina = (array)$gmina;
        return (array)$gmina;
    }

    static public function getGminaData($refresh = false)
    {
        $gmina = self::getGmina($refresh);
        if ($refresh == true && self::$fullGmina != null)
            return self::$fullGmina;
        
        global $wpdb;
        $cats = $wpdb->get_results( $wpdb->prepare('select * from '.$wpdb->prefix.'gmina_obiekty where nazwa_gminy = %s', $gmina->nazwa_gminy));
        foreach ($cats as $cat)
            $gmina->kategoria[] = array('name'=>$cat->Kategoria, 'ilosc'=>$cat->ilosc);

        $klubs = $wpdb->get_results( $wpdb->prepare('select * from '.$wpdb->prefix.'gmina_kluby where nazwa_gminy = %s', $gmina->nazwa_gminy));
        foreach ($klubs as $klub)
            $gmina->klub[] = array('name'=>$klub->Kategoria, 'ilosc'=>$klub->ilosc);
        
        $persons = $wpdb->get_results( $wpdb->prepare('select osoba from '.$wpdb->prefix.'gmina_persons where nazwa_gminy = %s', $gmina->nazwa_gminy));
        foreach ($persons as $person)
            $gmina->person[] = $person->osoba;
        self::$fullGmina = (array)$gmina;
        return (array)$gmina;
    }
    
    static public function saveGminaFromFORM($gmina)
    {
        if ($gmina == null) return;
//        var_dump($gmina);
        global $wpdb;
        $test = self::getGmina();
        if (!$test)
            return array('base'=>false);
//        var_dump($test);
//        if ($test['nazwa_gminy'] != $gmina['nazwa'])
//            return self::getGminaData();
        $persons = $gmina['person'];
        $kategorie = $gmina['kategoria'];
        $kluby = $gmina['klub'];
        
        $cat = array();
        foreach ($kategorie['nazwa'] as $key => $val)
        {
            $cat[] = array('name' => $val, 'ilosc' => $kategorie['ilosc'][$key]);
        }
        
        $klub = array();
        foreach ($kluby['nazwa'] as $key => $val)
        {
            $klub[] = array('name' => $val, 'ilosc' => $kluby['ilosc'][$key]);
        }

        $wpdb->query($wpdb->prepare('delete from '.$wpdb->prefix.'gmina_obiekty where nazwa_gminy = %s',$test->nazwa_gminy));
        $wpdb->query($wpdb->prepare('delete from '.$wpdb->prefix.'gmina_persons where nazwa_gminy = %s',$test->nazwa_gminy));
        $wpdb->query($wpdb->prepare('delete from '.$wpdb->prefix.'gmina_kluby   where nazwa_gminy = %s',$test->nazwa_gminy));

        $state['base'] = $wpdb->query($wpdb->prepare('replace into '.$wpdb->prefix.'gmina set isDeleted = isDeleted, nazwa = %s, mieszkancy = %s, typ = %s, nazwa_gminy = %s, kwota = %s, procenty = %s',$gmina['nazwa'],$gmina['mieszkancy'],$gmina['typ'],$test->nazwa_gminy, $gmina['kwota'], $gmina['procenty']));

        foreach ($klub as $klu)
        {
            $wpdb->prepare('replace into '.$wpdb->prefix.'gmina_kluby set nazwa_gminy = %s, Kategoria = %s, ilosc = %s', $test->nazwa_gminy, $klu['name'], $klu['ilosc']) . '<br>';
            $state['klubs'] = $wpdb->query($wpdb->prepare('replace into '.$wpdb->prefix.'gmina_kluby set nazwa_gminy = %s, Kategoria = %s, ilosc = %s', $test->nazwa_gminy, $klu['name'], $klu['ilosc']));
        }
        foreach ($cat as $catt)
            $state['kats'] = $wpdb->query($wpdb->prepare('replace into '.$wpdb->prefix.'gmina_obiekty set nazwa_gminy = %s, Kategoria = %s, ilosc = %s', $test->nazwa_gminy, $catt['name'], $catt['ilosc']));
        foreach ($persons as $person)
            $state['pers'] = $wpdb->query($wpdb->prepare('replace into '.$wpdb->prefix.'gmina_persons set nazwa_gminy = %s, osoba = %s',$test->nazwa_gminy,$person));

        return $state;
    }
    
    
    static public function addGminaFile($file)
    {
        global $current_user;
        $gmina = self::getGmina();
        $postID = wp_insert_post( array(
            'post_status' => 'private',
            'post_type' => 'gmina_file',
            'post_author' => $current_user->ID,
            'ping_status' => get_option('default_ping_status'),
            'post_parent' => 0,
//            'guid' => $file['fileName'],
            'post_content_filtered' => '',
            'import_id' => 0,
            'post_title' => $file['gmina'],
//            'post_name' => ,
            'post_content' => $file['realName'],
            'post_content_filtered' => $file['fileName'],
            'guid' => $file['path'],
            'post_excerp' => $file['excerp'],
            'post_mime_type' => $file['type'],
            'menu_order' => $file['menu'],
//            'post_excerpt' => $file['menu'],
//            'post_parent' => -$file['menu'],
//            'comment_count' => -$file['menu'],
        ));
        return $postID;
    }
    
    static public function removeGminaFile($file)
    {
        $att = get_post($file['id']);
        if ($att->menu_order == $file['part'] && $att->post_type == 'gmina_file' && $att->menu_order == $file['part'])
        {
            $gmina = self::getGmina();
            if ($gmina->nazwa_gminy == $att->post_title)
            {
                global $wpdb;
                $cont = explode('|', $att->post_content);
                unlink (ABSPATH . 'front_uploads/' . $gmina->nazwa_gminy .'/' . $cont[0]);
                $wpdb->delete($wpdb->prefix . 'posts', array('ID' => $att->ID));
                return $att->ID;
            }
            return -2;
        }
        return -1;
    }
    
    static public function getGminaAnkieta($part)
    {
        $gmina = self::getGmina();
        $args = array(
            'post_type' => 'gmina_ankieta',
            'posts_per_page' => -1,
            //'post_title' => 'Ankieta:'.$gmina->nazwa_gminy.':'.($part*1),
        );
        $posts = new WP_Query( $args );
        if ( $posts -> have_posts() )
        {
            $posts = $posts->get_posts();
            foreach ($posts as $post)
            {
#                echo '<pre>'.$post->ID.' / '.$post->post_title.' / '.('Ankieta:'.$gmina->nazwa_gminy.':'.($part*1)).'</pre>';
#                print_r($post->post_title == 'Ankieta:'.$gmina->nazwa_gminy.':'.($part*1));
                if (trim($post->post_title) == 'Ankieta:'.$gmina->nazwa_gminy.':'.($part*1) )
                    return $post;
            }
        }
//        else
        { //Utwórz posta
            $post = array(
                'post_status' => 'private',
                'post_type' => 'gmina_ankieta',
                'post_author' => $current_user->ID,
                'ping_status' => get_option('default_ping_status'),
                'post_parent' => 0,
                'post_content_filtered' => '',
                'import_id' => 0,
                'post_title' => 'Ankieta:'.$gmina->nazwa_gminy.':'.($part*1),
                'post_content' => '[[Odpowiedzi zapisz tutaj]]',
                'menu_order' => $part*1,
            );
            $id = wp_insert_post($post);
            return get_post($id);
        }
    }
    
    static public function saveGminaAnkieta($part, $tresc)
    {
        global $current_user;
        
        $gmina = self::getGmina();
        $args = array(
            'post_type' => 'gmina_ankieta',
            'posts_per_page' => -1,
            'post_author' => $current_user->ID,
            //'post_title' => 'Ankieta:'.$gmina->nazwa_gminy.':'.($part*1),
        );
//        var_dump($args);
        $posts = new WP_Query( $args );
        $updated = false;
        if ( $posts -> have_posts() )
        {
            $ankieta = null;
            $posts = $posts->get_posts();
            #if (count($posts) > 1) die('{message: "Błąd istnienia ankiety. Potrzebna pomoc administratora..."}');
            foreach ($posts as $post)
            {
                if ($post->post_author != $current_user->ID) continue;
//                echo $post->post_title;
                $t = explode(':', $post->post_title);
                if ($t[2] == $part*1 && substr($post->post_title,0,strlen('Ankieta:'.$gmina->nazwa_gminy.':'.($part*1))) == 'Ankieta:'.$gmina->nazwa_gminy.':'.($part*1))
                {
//                    echo ' - TRUE';
                    $ankieta = $post;
                    $updated = true;
                    break;
                }
//                echo "\n";
            }
            
            if ($ankieta != null)
            {
                $post = $ankieta;
                $oldTresc = $post->post_content;
                $post->post_content = $tresc;
                $id = wp_update_post($post);

                if ($tresc != $oldTresc)
                {                
                    $post->ID = null;
                    $post->post_type = 'revision';
                    $post->post_parent = $id;
                    $post->post_status = 'inherit';
                    $post->post_date = current_time('mysql');
                    wp_insert_post($post);
                }
                return $id;
            }
            elseif ($updated)
                return false;
        }
        if (!$updated)
        { //Utwórz posta
            $post = array(
                'post_status' => 'private',
                'post_type' => 'gmina_ankieta',
                'post_author' => $current_user->ID,
                'ping_status' => get_option('default_ping_status'),
                'post_parent' => 0,
                'post_content_filtered' => '',
                'import_id' => 0,
                'post_title' => 'Ankieta:'.$gmina->nazwa_gminy.':'.($part*1),
                'post_content' => $tresc,
                'menu_order' => $part*1,
            );
            return $id = wp_insert_post($post);
        }

    }
}

//Function that just created the object and ignores more...
function initialize_setiWatchdogUsers()
{
    new setiWatchdogUsers();
}
add_action('init', array('setiWatchdogUsers','register_custom_posts'));
add_action('plugins_loaded', 'initialize_setiWatchdogUsers');
add_action('activate_' . plugin_basename( __FILE__ ), array('setiWatchdogUsers','install'), 1);
//add_action('admin_init', array('setiWatchdogUsers','install'), 1);

